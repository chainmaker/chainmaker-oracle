/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

CREATE user 'test'@'%' identified by 'Wei123Xin@';
GRANT All PRIVILEGES ON *.* TO 'test'@'%';
flush privileges;



CREATE DATABASE if NOT exists `chain_oracle` ;

use `chain_oracle`;

CREATE TABLE  if NOT exists `oracle_contracts`(
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `chain_alias` varchar(128) NOT NULL COMMENT '链别名',
  `chain_id` varchar(128) NOT NULL COMMENT '链id',
  `contract_name` varchar(128) NOT NULL COMMENT '合约名称',
  `contract_version` int NOT NULL DEFAULT '1' COMMENT '合约版本',
  `deployed` tinyint NOT NULL DEFAULT '0' COMMENT '是否部署过，默认为0未部署，1为已经部署',
  `runtime_type` tinyint NOT NULL DEFAULT '6' COMMENT '1 为系统合约；2 为rust; 3 为c++; 4为tinygo; 5为solidity; 6为go',
  `topic_query_start` bigint unsigned NOT NULL COMMENT '订阅“查询事件”的起始位置',
  `topic_finish_start` bigint unsigned NOT NULL COMMENT '订阅“结束事件”的起始位置',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_c_c` (`chain_alias`,`contract_name`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='合约情况表格'; 

CREATE TABLE  if NOT exists `event_state`(
  `id` bigint unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `chain_alias` varchar(128) NOT NULL,
  `request_id` varchar(256) NOT NULL COMMENT '请求id',
  `tx_id` varchar(128) NOT NULL COMMENT '交易id',
  `contract_name` varchar(128) NOT NULL COMMENT '合约名称',
  `state` tinyint NOT NULL DEFAULT '0' COMMENT '0：StateInit ；1：StateFetchDataFailed ； 2： StateFetchDataSucess ； 3：  StateCallbackFailed ； 4：StateCallbackSuccess ； 5：StateResultOnChainSuccess（终止态） ；6：StateCallbackFailedFibnoacci（多次调链失败终止态）',
  `query_type` tinyint NOT NULL DEFAULT '1' COMMENT '查询类型枚举 1：use_http；2：use_db ',
  `query_body` mediumblob NOT NULL COMMENT '具体的查询方案，暂定为序列化后的json字符串',
  `next_fibonacci` bigint unsigned NOT NULL DEFAULT '0' COMMENT '上链失败时候feibonacci重试记录',
  `block_height` bigint unsigned NOT NULL DEFAULT '1' COMMENT '当前事件的区块高度',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq_s_c_r` (`request_id`) COMMENT '''请求id唯一索引''',
  KEY `updated_state` (`updated_at`,`state`),
  KEY `created_state` (`created_at`,`state`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='事件状态表格' ;

CREATE TABLE if NOT exists `event_result` (
  `request_id` varchar(256) NOT NULL COMMENT '请求id，唯一索引',
  `event_result` mediumblob NOT NULL COMMENT '取到的数据',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '链id',
  PRIMARY KEY (`request_id`),
  KEY `created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='预言机取数据返回值存储表' ;

CREATE TABLE if NOT exists `event_lock` (
  `request_id` varchar(256) NOT NULL COMMENT '请求id',
  `locked_version` int NOT NULL DEFAULT '0' COMMENT '锁',
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时记录',
  PRIMARY KEY (`request_id`) COMMENT '请求id唯一索引',
  KEY `updated_at` (`updated_at`),
  KEY `request_updated` (`request_id`,`updated_at`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='事件锁' ; 

CREATE TABLE if NOT exists  `vrfs`(
  `id` int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `privatek` blob NOT NULL COMMENT '私钥',
  `publick` blob NOT NULL COMMENT '公钥',
  `srv_name` varchar(128) NOT NULL COMMENT '服务名称，唯一索引',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq-s` (`srv_name`) COMMENT '利用服务名称作唯一索引'
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='vrf表格';