/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

// Package http_server_process 预言机的接口模块，提供http接口服务
package http_server_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/contract_process"
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

// Parameter http合约接口参数模型
type Parameter struct {
	ChainAlias       string
	ContractName     string
	ContractVersion  int
	ContractFilePath string
	Runtime          int32
}

// parseFormAndCheck 合约安装、升级接口参数校验
func parseFormAndCheck(cGin *gin.Context) (*Parameter, error) {
	// 管理功能，不再加中间件，直接在这里拦截好了
	adminAuthToken := cGin.GetHeader("admin_auth_token")
	if len(adminAuthToken) == 0 || adminAuthToken != config.GlobalCFG.HttpServerCFG.AdminToken {
		cGin.JSONP(http.StatusBadRequest, gin.H{
			"code":    400,
			"message": "you have no authority",
		})
		return nil, errors.New("have no authority")
	}

	chainAlias := strings.TrimSpace(cGin.PostForm("chain_alias"))
	if len(chainAlias) == 0 {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": "chain_alias missed",
		})
		return nil, errors.New("chain_alias missed")
	}
	contractVersion, cErr := strconv.Atoi(cGin.PostForm("contract_version"))
	if cErr != nil || contractVersion < 1 {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": "contract_version must be integer larger than 0",
		})
		return nil, errors.New("contract_version must be integer larger than 0")
	}
	runtimeValid, runtime := contract_process.CheckRuntimeParam(strings.TrimSpace(cGin.PostForm("runtime")))
	if !runtimeValid {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": "runtime must be one of the follows : NATIVE  WASMER  WXVM GASM EVM DOCKER_GO",
		})
		return nil, errors.New("runtime must be one of the follows : NATIVE  WASMER  WXVM GASM EVM DOCKER_GO")
	}

	cFile, cFileErr := cGin.FormFile("contract_file")
	if cFileErr != nil || cFile == nil {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": "contract_file not exists",
		})
		return nil, errors.New("contract_file not exists")
	}
	contractName := config.GlobalCFG.WorkCFG.OracleContractName // 这个是配置中的文件名称
	dir, contractFilePath := makeContractDir(chainAlias, contractName, contractVersion)
	saveErr := saveFile(cFile, dir, contractFilePath)
	if saveErr != nil {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": fmt.Sprintf("upload file err(%s)", saveErr.Error()),
		})
		return nil, fmt.Errorf("upload file err(%s)", saveErr.Error())
	}
	return &Parameter{
		ChainAlias:       chainAlias,
		ContractName:     contractName,
		ContractVersion:  contractVersion,
		ContractFilePath: contractFilePath,
		Runtime:          runtime,
	}, nil
}

// makeContractDir 根据链，合约名称，版本构造文件夹
func makeContractDir(chainAlias, contractName string, contractVersion int) (string, string) {
	dir := fmt.Sprintf("./contract_files/%s/%s/%d", chainAlias, contractName, contractVersion)
	return dir, fmt.Sprintf("%s/%s", dir, fmt.Sprintf("%s.7z", contractName))
}

// saveFile 保存合约文件到指定目录下
// @param *multipart.FileHeader
// @param string
// @param string
// @return error
func saveFile(fileHeader *multipart.FileHeader, fileDir, filePath string) error {
	sourceF, sourceFErr := fileHeader.Open()
	if sourceFErr != nil {
		return sourceFErr
	}
	defer sourceF.Close()
	dirErr := os.MkdirAll(fileDir, 0777)
	if dirErr != nil {
		return dirErr
	}
	targetF, targetFErr := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE|os.O_EXCL, 0666)
	if targetFErr != nil {
		return targetFErr
	}
	defer targetF.Close()
	_, cpErr := io.Copy(targetF, sourceF)
	if cpErr != nil {
		return cpErr
	}
	return nil
}

func checkAssistAuth(cGin *gin.Context) error {
	assistToken := cGin.GetHeader("assist_auth_token")
	if len(assistToken) == 0 || assistToken != config.GlobalCFG.HttpServerCFG.AssistToken {
		cGin.JSONP(http.StatusBadRequest, gin.H{
			"code":    400,
			"message": "you have no authority",
		})
		return errors.New("have no authority")
	}
	return nil
}
