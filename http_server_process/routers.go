/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package http_server_process

import (
	"chainmaker-oracle/contract_process/vrf_utils"
	"chainmaker-oracle/fetch_data"
	"chainmaker-oracle/models"
	"chainmaker-oracle/models/cross_chain"
	"chainmaker-oracle/models/smart_http"
	"chainmaker-oracle/models/smart_sql"
	"encoding/json"
	"fmt"
	"math/big"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	"github.com/gin-gonic/gin"
)

// postInstallContract 安装合约接口
func (srv *HttpSrv) postInstallContract(cGin *gin.Context) {
	// 校验参数
	parameter, err := parseFormAndCheck(cGin)
	if err != nil {
		return
	}
	// 安装合约
	installed, installedErr := srv.BusinessProcessor.DeployContractAndSubScribe(parameter.ChainAlias,
		parameter.ContractName, strconv.Itoa(parameter.ContractVersion),
		parameter.ContractFilePath, common.RuntimeType(parameter.Runtime))
	if installedErr != nil {
		_ = os.Remove(parameter.ContractFilePath)
		_ = os.Remove(filepath.Dir(parameter.ContractFilePath))
		srv.Log.Debugf("PostInstallContract filepath(%s), fileDir(%s)  ",
			parameter.ContractFilePath, filepath.Dir(parameter.ContractFilePath))
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": fmt.Sprintf("install contract err (%s)  ", installedErr.Error()),
		})
		return
	}
	cGin.JSONP(http.StatusOK, gin.H{
		"code":    0,
		"message": "success",
		"data":    installed,
	})
}

// postUpgradeContract 升级合约接口
func (srv *HttpSrv) postUpgradeContract(cGin *gin.Context) {
	// 校验参数
	parameter, err := parseFormAndCheck(cGin)
	if err != nil {
		return
	}
	// 升级合约
	updateSuccess, updateErr := srv.BusinessProcessor.UpdateContractAndUpdateDB(parameter.ContractName,
		parameter.ContractFilePath, parameter.ChainAlias, parameter.ContractVersion, 2)
	if updateErr != nil || !updateSuccess {
		_ = os.Remove(parameter.ContractFilePath)
		_ = os.Remove(filepath.Dir(parameter.ContractFilePath))
		cGin.JSONP(http.StatusOK, gin.H{
			"code": 400,
			"message": fmt.Sprintf("upgrade contract success(%+v), err (%+v), contract(%+v)  ",
				updateSuccess, updateErr, parameter),
		})
		return
	}
	cGin.JSONP(http.StatusOK, gin.H{
		"code":    0,
		"message": "success",
		"data":    parameter,
	})

}

// getContractList 查询合约接口
func (srv *HttpSrv) getContractList(cGin *gin.Context) {
	// 校验参数（主要是鉴权）
	authErr := checkAssistAuth(cGin)
	if authErr != nil {
		return
	}
	// 取链参数
	chainAlias, chainAliasOk := cGin.GetQuery("chain_alias")
	if !chainAliasOk {
		srv.Log.Warn("chain_alias missed")
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": "chain_alias missed",
		})
		return
	}
	// 查询链上安装的预言机合约
	contracts, queryErr := srv.BusinessProcessor.ListInstalledOracleContracts(chainAlias)
	if queryErr != nil {
		srv.Log.Errorf("QueryOracleContracts error, %s", queryErr.Error())
		cGin.JSONP(http.StatusInternalServerError, gin.H{
			"code":    500,
			"message": "server busy ,try later",
		})
		return
	}
	cGin.JSONP(http.StatusOK, gin.H{
		"code":    0,
		"message": "success",
		"data":    contracts,
	})
}

// healthCheck 健康检查接口
func (srv *HttpSrv) healthCheck(cGin *gin.Context) {
	cGin.JSONP(http.StatusOK, gin.H{
		"code":    0,
		"message": "success",
	})
}

// getPublicKey 获取预言机公钥接口
func (srv *HttpSrv) getPublicKey(cGin *gin.Context) {
	// 鉴权
	authErr := checkAssistAuth(cGin)
	if authErr != nil {
		return
	}
	pk := vrf_utils.GlobalECVRF.PublicKeyBytes
	cGin.JSONP(http.StatusOK, gin.H{
		"code":    0,
		"message": "success",
		"data":    pk,
	})
}

// postVerify VRF校验接口
func (srv *HttpSrv) postVerify(cGin *gin.Context) {
	// 鉴权
	authErr := checkAssistAuth(cGin)
	if authErr != nil {
		return
	}
	var vp models.Verify
	bindErr := cGin.BindJSON(&vp)
	// 校验参数
	if bindErr != nil {
		srv.Log.Error("param error %s ", bindErr.Error())
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    0,
			"message": bindErr.Error(),
		})
		return
	}
	// 验证VRF
	success, sErr := vrf_utils.Verify(vp.Pi, []byte(vp.Alpha))
	retStr := "true"
	if sErr != nil || !success {
		retStr = "false"
	}
	if sErr != nil {

		srv.Log.Warnf("Verify error %s, pi (%s) ,alpha(%s) ", sErr, string(vp.Pi), vp.Alpha)
	}
	cGin.JSONP(http.StatusOK, gin.H{
		"code":    0,
		"message": "success",
		"data":    retStr,
	})
}

// postRandom 生成VRF接口
func (srv *HttpSrv) postRandom(cGin *gin.Context) {
	// 鉴权
	authErr := checkAssistAuth(cGin)
	if authErr != nil {
		return
	}
	alphaStr := cGin.PostForm("alpha")
	// 计算VRF
	pio, hash, proveErr := vrf_utils.Prove([]byte(alphaStr))
	if proveErr != nil {
		srv.Log.Warnf("PostRandom error %s, alpha(%s) ", proveErr.Error(), alphaStr)
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": proveErr.Error(),
		})
		return
	}
	var rr models.Random
	var bn big.Int
	bns := bn.SetBytes(hash)
	rr.RandData = bns.String()
	rr.Pi = pio
	// 计算一下[0,1]之间的随机数
	rr.Ratio, _ = vrf_utils.ComputeRatioFromHash(hash)
	cGin.JSONP(http.StatusOK, gin.H{
		"code":    0,
		"message": "success",
		"data":    rr,
	})
}

// getMysqlQuery 查询mysql数据接口
func (srv *HttpSrv) getMysqlQuery(cGin *gin.Context) {
	// 鉴权
	authErr := checkAssistAuth(cGin)
	if authErr != nil {
		return
	}
	var queryM smart_sql.ModelSql
	// 校验参数
	queryErr := cGin.BindJSON(&queryM)
	if queryErr != nil {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": "query_body can not be empty, " + queryErr.Error(),
		})
		return
	}
	bs, _ := json.Marshal(queryM)
	fetcher := fetch_data.FetchDatabase{}
	// 取数据
	data, dataErr := fetcher.Fetch(string(bs))
	if dataErr != nil {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": dataErr.Error(),
		})
		return
	}
	cGin.JSONP(http.StatusOK, gin.H{
		"result": string(data),
	})

}

// getHttpQuery 查询http接口
func (srv *HttpSrv) getHttpQuery(cGin *gin.Context) {
	// 鉴权
	authErr := checkAssistAuth(cGin)
	if authErr != nil {
		return
	}
	var queryM smart_http.ModelHttp
	// 校验参数
	queryErr := cGin.BindJSON(&queryM)
	if queryErr != nil {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": "query_body can not be empty, " + queryErr.Error(),
		})
		return
	}
	bs, _ := json.Marshal(queryM)
	fetcher := fetch_data.FetchHttp{}
	// 取http接口数据
	data, dataErr := fetcher.Fetch(string(bs))
	if dataErr != nil {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": dataErr.Error(),
		})
		return
	}
	cGin.JSONP(http.StatusOK, gin.H{
		"result": string(data),
	})
}

// getCrossQuery 跨链查询接口
func (srv *HttpSrv) getCrossQuery(cGin *gin.Context) {
	// 鉴权
	authErr := checkAssistAuth(cGin)
	if authErr != nil {
		return
	}
	var queryM cross_chain.CrossChainQuery
	// 校验参数
	queryErr := cGin.BindJSON(&queryM)
	if queryErr != nil {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": "query_body can not be empty, " + queryErr.Error(),
		})
		return
	}
	bs, _ := json.Marshal(queryM)
	// 取一下跨链查询数据
	data, dataErr := srv.BusinessProcessor.Fetch(string(bs))
	if dataErr != nil {
		cGin.JSONP(http.StatusOK, gin.H{
			"code":    400,
			"message": dataErr.Error(),
		})
		return
	}
	cGin.JSONP(http.StatusOK, gin.H{
		"result": string(data),
	})
}
