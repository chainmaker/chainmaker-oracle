FROM golang:1.16 as build

WORKDIR /build-tmp

COPY . . 


RUN go env -w GOPROXY=https://goproxy.cn,direct && go build 

WORKDIR /build 

RUN  cp /build-tmp/install-upgrade-oracle-contract.sh . \ 
&& chmod +x install-upgrade-oracle-contract.sh \ 
&& cp -r /build-tmp/chainmaker-oracle . \
&& cp -r /build-tmp/standard_oracle_contract . 

FROM ubuntu:20.04 


RUN apt-get -qq update \
    && apt-get -qq install -y --no-install-recommends ca-certificates curl \ 
    &&  apt-get -qy install netcat \
    && apt-get -qy install p7zip-full \
    && apt-get install -qy tzdata \
    && ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime \ 
    && echo 'Asia/Shanghai'>/etc/timezone 

WORKDIR /chainmaker-oracle 
COPY --from=build  /build . 



