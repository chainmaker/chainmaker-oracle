/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/models"
	"strings"

	"chainmaker.org/chainmaker/pb-go/v2/common"
)

// fibonacciNumber 计算n对应的fibonacci数字
// @param int
// @param int64
func fibonacciNumber(n int) int64 {
	if n == 0 || n == 1 {
		return int64(n)
	}

	return fibonacciNumber(n-1) + fibonacciNumber(n-2)
}

// fibonacciDupNumber 计算n对应的fibonacci数字(高效率的)
// @param int
// @param []int64
// @param int64
func fibonacciDupNumber(n int, array []int64) int64 {
	if n == 0 {
		array[0] = 0
		return 0
	}
	if n == 1 {
		array[1] = 1
		return 1
	}
	if array[n] > 0 {
		return array[n]
	}
	array[n] = fibonacciDupNumber(n-1, array) + fibonacciDupNumber(n-2, array)
	return array[n]
}

// CheckRuntimeParam 校验runtime参数
// @param string
// @return bool
// @return int32
func CheckRuntimeParam(runtime string) (bool, int32) {
	formalRuntime, ok := common.RuntimeType_value[strings.ToUpper(runtime)]
	if !ok {
		return false, -1
	}
	return true, formalRuntime
}

// makeContractDeployOrUpgradeKV 构造安装升级预言机合约参数
// @param string
// @param string
// @param string
// @param []byte
// @return []*common.KeyValuePair
func makeContractDeployOrUpgradeKV(chainAlias, version, name string, publicKeyBs []byte) []*common.KeyValuePair {
	// 构造安装、升级合约时候传递的参数
	return []*common.KeyValuePair{
		{
			Key:   "chainAlias",
			Value: []byte(chainAlias),
		},
		{
			Key:   "version",
			Value: []byte(version),
		},
		{
			Key:   "curvePublicKey",
			Value: publicKeyBs,
		},
		{
			Key:   "name",
			Value: []byte(name),
		},
	}
}

// makeDbContractFromContractEntry 根据前端传入的合约参数构造合约信息
// @param ContractFileEntry
// @return *models.ContractInfo
func makeDbContractFromContractEntry(entry ContractFileEntry) *models.ContractInfo {
	// 构造数据库的合约模型
	return &models.ContractInfo{
		ChainAlias:       entry.ChainAlias,
		ChainId:          entry.ChainId,
		ContractName:     entry.ContractName,
		ContractVersion:  entry.Version,
		Deployed:         1,
		RuntimeType:      int(common.RuntimeType_DOCKER_GO),
		TopicQueryStart:  1,
		TopicFinishStart: 1,
	}
}
