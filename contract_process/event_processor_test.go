/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/models"
	"chainmaker-oracle/models/cross_chain"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"reflect"
	"testing"
	"time"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	cfg "chainmaker.org/chainmaker/pb-go/v2/config"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	. "github.com/agiledragon/gomonkey"
	"github.com/robfig/cron/v3"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDealEvent(t *testing.T) {
	Convey("dealEvent", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		patchjob := ApplyMethod(reflect.TypeOf(processor.CronManager), "AddJob", func(*cron.Cron, string, cron.Job) (cron.EntryID, error) {
			return cron.EntryID(1), nil
		})
		defer patchjob.Reset()
		Convey("query", func() {
			patch2 := ApplyMethod(reflect.TypeOf(processor), "ProcessEventQuery", func(*ContractProcessor, *common.ContractEventInfo) {

			})
			defer patch2.Reset()
			processor.dealEvent(&common.ContractEventInfo{Topic: topicOracleQuery})
		})
		Convey("finish", func() {
			patch3 := ApplyMethod(reflect.TypeOf(processor), "ProcessEventFinished", func(*ContractProcessor, *common.ContractEventInfo) {

			})
			defer patch3.Reset()
			processor.dealEvent(&common.ContractEventInfo{Topic: topicOracleFinished})
		})
	})
}

func TestProcessEventQuery(t *testing.T) {
	Convey("ProcessEventQuery", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		patchjob := ApplyMethod(reflect.TypeOf(processor.CronManager), "AddJob", func(*cron.Cron, string, cron.Job) (cron.EntryID, error) {
			return cron.EntryID(1), nil
		})
		defer patchjob.Reset()
		Convey("addjob", func() {
			patch2 := ApplyFunc(models.InsertEventLock, func(*models.EventLock) error {
				return nil
			})
			defer patch2.Reset()
			patch3 := ApplyFunc(models.InsertEventState, func(*models.EventState) error {
				return errors.New("mysql db close")
			})
			defer patch3.Reset()
			processor.ProcessEventQuery(&common.ContractEventInfo{
				BlockHeight:  1,
				ChainId:      "chain1_alias",
				Topic:        "oracle_query",
				TxId:         "ajfi23e2djafvbsj",
				ContractName: "oracle_contract_v1",
				EventData:    []string{"query_mysql", "chain1_alias", "jisj23", "sadjf123", "1", "select * from employee"},
			})
		})

		Convey("runjob", func() {
			patch4 := ApplyFunc(models.InsertEventLock, func(*models.EventLock) error {
				return nil
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.InsertEventState, func(*models.EventState) error {
				return nil
			})
			defer patch5.Reset()
			patch6 := ApplyFunc(models.UpdateContractTopicQueryStart, func(string, string, uint64) error {
				return nil
			})
			defer patch6.Reset()
			patch7 := ApplyMethod(reflect.TypeOf(processor), "Run", func(*ContractProcessor, string, string, string) error {
				return nil
			})
			defer patch7.Reset()
			processor.ProcessEventQuery(&common.ContractEventInfo{
				BlockHeight:  1,
				ChainId:      "chain1_alias",
				Topic:        "oracle_query",
				TxId:         "ajfi23e2djafvbsj",
				ContractName: "oracle_contract_v1",
				EventData:    []string{"query_mysql", "chain1_alias", "jisj23", "sadjf123", "1", "select * from employee"},
			})
		})

	})
}

func TestProcessEventFinished(t *testing.T) {
	Convey("ProcessEventFinished", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		patchjob := ApplyMethod(reflect.TypeOf(processor.CronManager), "AddJob", func(*cron.Cron, string, cron.Job) (cron.EntryID, error) {
			return cron.EntryID(1), nil
		})
		defer patchjob.Reset()
		Convey("onChain", func() {
			patch2 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
				return &models.EventState{State: models.StateResultOnChainSuccess}, nil
			})
			defer patch2.Reset()
			processor.ProcessEventFinished(&common.ContractEventInfo{
				BlockHeight:  1,
				ChainId:      "chain1_alias",
				Topic:        "oracle_finished",
				TxId:         "ajfi23e2djafvbsj",
				ContractName: "oracle_contract_v1",
				EventData:    []string{"chain1_alias", "jisj23", "sadjf123", "1"},
			})
		})
		Convey("updateTopic", func() {
			patch3 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
				return &models.EventState{State: models.StateFetchDataSuccess}, nil
			})
			defer patch3.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateContractTopicFinishStart, func(string, string, uint64) error {
				return nil
			})
			defer patch5.Reset()
			processor.ProcessEventFinished(&common.ContractEventInfo{
				BlockHeight:  1,
				ChainId:      "chain1_alias",
				Topic:        "oracle_finished",
				TxId:         "ajfi23e2djafvbsj",
				ContractName: "oracle_contract_v1",
				EventData:    []string{"chain1_alias", "jisj23", "sadjf123", "1"},
			})
		})
	})
}

func TestMakeDBEventStateFromEvent(t *testing.T) {
	Convey("makeDBEventStateFromEvent", t, func() {
		makeDBEventStateFromEvent(&common.ContractEventInfo{
			BlockHeight:  1,
			ChainId:      "chain1_alias",
			Topic:        "oracle_query",
			TxId:         "ajfi23e2djafvbsj",
			ContractName: "oracle_contract_v1",
			EventData:    []string{"query_mysql", "chain1_alias", "jisj23", "sadjf123", "1", "select * from employee"},
		})
	})
}

func TestRemoveCronJob(t *testing.T) {

	Convey("RemoveCronJob", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		key := models.ComputeHashKey("chain1_alias", "adsf123", "zsbd230kjl")
		processor.EventInProcess.Store(key, EventV{
			EntryId: 12,
			State:   globalJobIsCron,
		})
		processor.RemoveCronJob("chain1_alias", "adsf123", "zsbd230kjl")
	})

}

func TestFetch(t *testing.T) {
	Convey("Fetch", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		patchjob := ApplyMethod(reflect.TypeOf(processor.CronManager), "AddJob", func(*cron.Cron, string, cron.Job) (cron.EntryID, error) {
			return cron.EntryID(1), nil
		})
		defer patchjob.Reset()
		Convey("success", func() {
			patch2 := ApplyMethod(reflect.TypeOf(processor),
				"QueryContract", func(*ContractProcessor,
					string, string, string,
					[]*common.KeyValuePair) (*common.TxResponse, error) {
					return &common.TxResponse{
						ContractResult: &common.ContractResult{
							Code:   0,
							Result: []byte("success"),
						},
					}, nil
				})
			defer patch2.Reset()
			fetchcfg := cross_chain.CrossChainQuery{
				ChainAlias:   "chain2_alias",
				ContractName: "use_demo",
				MethodName:   "queryByHash",
			}
			bs, _ := json.Marshal(fetchcfg)
			_, err := processor.Fetch(string(bs))
			So(err, ShouldBeNil)
		})
	})
}

func TestToString(t *testing.T) {
	Convey("ToString", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}

		wJob := &WatchCronJobs{
			JobType:    watchTypeCron,
			ChainAlias: "chain1_alias",
			RequestId:  "djfa023jkdfa",
			TxId:       "ijwek8723jfsa",
			Processor:  processor,
		}
		fmt.Println(wJob.ToString(time.Now()))
	})
}

// func TestJobRun(t *testing.T) {
// 	Convey("ToString", t, func() {
// 		config.ReadConfigFile("../config_files/smart_oracle.yml")
// 		os.Chdir("../")
// 		patch := ApplyFunc(sdk.NewChainClient,
// 			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
// 				return &sdk.ChainClient{}, nil
// 			})
// 		clientP := &sdk.ChainClient{}
// 		defer patch.Reset()
// 		patch1 := ApplyMethod(reflect.TypeOf(clientP),
// 			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
// 				return &cfg.ChainConfig{}, nil
// 			})
// 		defer patch1.Reset()
// 		processor, err := NewContractProcessor()
// 		if err != nil {
// 			return
// 		}

// 		wJob := &WatchCronJobs{
// 			JobType:    watchTypeCron,
// 			ChainAlias: "chain1_alias",
// 			RequestId:  "djfa023jkdfa",
// 			TxId:       "ijwek8723jfsa",
// 			Processor:  processor,
// 		}
// 		Convey("monitorTypeContract", func() {
// 			patch2 := ApplyMethod(reflect.TypeOf(processor),
// 				"SubscribeContractEvents", func(*ContractProcesor) error {
// 					return nil
// 				})
// 			defer patch2.Reset()
// 			wJob.JobType = monitorTypeContract
// 			wJob.Run()
// 		})
// 		Convey("return", func() {
// 			patch3 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
// 				return &models.EventState{
// 					State: models.StateCallbackSuccess,
// 				}, nil
// 			})
// 			defer patch3.Reset()
// 			wJob.JobType = globalJobIsCron
// 			wJob.Run()
// 		})
// 		Convey("fibonacci", func() {
// 			patch4 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
// 				return &models.EventState{
// 					State: models.StateCallbackFailed,
// 				}, nil
// 			})
// 			defer patch4.Reset()
// 			wJob.JobType = watchTypeFibonacci
// 			patch5 := ApplyMethod(reflect.TypeOf(processor), "Run", func(*ContractProcesor, string, string, string) error {
// 				return nil
// 			})
// 			defer patch5.Reset()
// 			wJob.Run()
// 		})
// 		Convey("jobRun", func() {
// 			patch6 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
// 				return &models.EventState{
// 					State: models.StateFetchDataSuccess,
// 				}, nil
// 			})
// 			defer patch6.Reset()
// 			wJob.JobType = watchTypeCron
// 			patch7 := ApplyMethod(reflect.TypeOf(processor), "Run", func(*ContractProcesor, string, string, string) error {
// 				return nil
// 			})
// 			defer patch7.Reset()
// 			patch8 := ApplyFunc(models.UpdateEventLock, func(*models.EventLock) error {
// 				return nil
// 			})
// 			defer patch8.Reset()
// 			wJob.Run()
// 		})
// 	})
// }
