/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/contract_process/vrf_utils"
	"chainmaker-oracle/models"
	"fmt"
	"os"
	"testing"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	sdkutils "chainmaker.org/chainmaker/sdk-go/v2/utils"
	. "github.com/agiledragon/gomonkey"

	. "github.com/smartystreets/goconvey/convey"
)

func TestGetSdkConfigByChainAlias(t *testing.T) {
	config.ReadConfigFile("../config_files/smart_oracle.yml")
	chains := []string{"chain1", "chain2"}
	fmt.Println("==========")
	for i := 0; i < len(chains); i++ {
		tcfg, tbool := getSdkConfigByChainAlias(chains[i])
		if !tbool {
			continue
		}
		fmt.Printf("chain(%s), cfgs(%+v)  ", chains[i], *tcfg)
	}
}

func TestScanDbContracts(t *testing.T) {
	Convey("ScanDbContracts", t, func() {
		Convey("success", func() {
			ApplyFunc(models.LoadContracts, func() ([]models.ContractInfo, error) {
				return []models.ContractInfo{
					{
						ChainAlias:      "chain1_alias",
						ChainId:         "chain1",
						ContractName:    "oracle_contract_v1",
						ContractVersion: 1,
						Deployed:        1,
						RuntimeType:     6,
					},
					{
						ChainAlias:      "chain2_alias",
						ChainId:         "chain2",
						ContractName:    "oracle_contract_v1",
						ContractVersion: 1,
						Deployed:        1,
						RuntimeType:     6,
					},
				}, nil
			})
			contracts, err := ScanDbContracts()
			So(err, ShouldBeNil)
			So(len(contracts), ShouldEqual, 2)
		})
	})
}

func TestCheckProposalRequestResp(t *testing.T) {
	CheckProposalRequestResp(&common.TxResponse{
		Code:    0,
		Message: "",
		TxId:    "sdfjl89uafdjiua12",
		ContractResult: &common.ContractResult{
			Code:    200,
			Result:  []byte("success"),
			Message: "ok",
		},
	}, true)
}

func TestGetEndorsersWithAuthType(t *testing.T) {
	Convey("GetEndorsersWithAuthType", t, func() {
		patch1 := ApplyFunc(sdkutils.MakeEndorserWithPath,
			func(keyFilePath, crtFilePath string,
				payload *common.Payload) (*common.EndorsementEntry, error) {
				return &common.EndorsementEntry{}, nil
			})
		defer patch1.Reset()
		patch2 := ApplyFunc(sdkutils.MakePkEndorserWithPath,
			func(keyFilePath string, hashType crypto.HashType, orgId string,
				payload *common.Payload) (*common.EndorsementEntry, error) {
				return &common.EndorsementEntry{}, nil
			})
		defer patch2.Reset()

		Convey("success.PermissionedWithCert", func() {
			_, err := GetEndorsersWithAuthType(1, sdk.PermissionedWithCert, &common.Payload{}, nil)
			So(err, ShouldBeNil)
		})
		Convey("success.PermissionedWithKey", func() {
			_, err := GetEndorsersWithAuthType(1, sdk.PermissionedWithKey, &common.Payload{}, nil)
			So(err, ShouldBeNil)
		})
		Convey("success.Public", func() {
			_, err := GetEndorsersWithAuthType(1, sdk.Public, &common.Payload{}, nil)
			So(err, ShouldBeNil)
		})

	})
}

func TestCreateChainClientWithSDKConf(t *testing.T) {
	Convey("CreateChainClientWithSDKConf", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		vrf_utils.GlobalECVRF = &vrf_utils.ECVRF{
			PublicKeyBytes:  []byte("sji23sdvkn"),
			PrivateKeyBytes: []byte("27r2jfkajjkf"),
		}
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		Convey("success", func() {
			_, err := CreateChainClientWithSDKConf("../config_files/sdk-config/sdk_config_chain1.yml")
			So(err, ShouldBeNil)
		})
	})
}

func TestNewContractProcesser(t *testing.T) {
	Convey("NewContractProcessor", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		vrf_utils.GlobalECVRF = &vrf_utils.ECVRF{
			PublicKeyBytes:  []byte("sji23sdvkn"),
			PrivateKeyBytes: []byte("27r2jfkajjkf"),
		}
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		Convey("success", func() {
			_, err := NewContractProcessor()
			So(err, ShouldBeNil)
		})
	})
}
