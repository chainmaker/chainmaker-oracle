/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

// Package contract_process 预言机核心服务实现
package contract_process

// MonitorContract 增加一个定时任务，轮询安装合约的数据库
func (cp *ContractProcessor) MonitorContract() {
	wJob := &WatchCronJobs{
		JobType:   monitorTypeContract,
		Processor: cp,
	}
	// 加一个定时任务，轮训合约安装的数据库表格
	entryId, _ := cp.ContractCronManager.AddJob(cp.MonitorContractTemplate, wJob)
	wJob.EntryId = entryId
}
