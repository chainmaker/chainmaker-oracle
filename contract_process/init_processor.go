/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/logger"
	"chainmaker-oracle/models"
	"errors"
	"fmt"
	"sync"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	sdkutils "chainmaker.org/chainmaker/sdk-go/v2/utils"
	"github.com/panjf2000/ants/v2"
	"github.com/robfig/cron/v3"
	"go.uber.org/zap"
	"golang.org/x/sync/singleflight"
)

var (
	topicOracleQuery     = "oracle_query"    // topicOracleQuery 查询事件监控
	topicOracleFinished  = "oracle_finished" // topicOracleFinished 完成事件监控
	invokeMethod         = "invoke_contract" // invokeMethod 执行合约
	callbackMethod       = "callback"        // callbackMethod 回调方法
	cronStandardTemplate = "%d %d %d %d %d ?"
	// cronStandardTemplate second minute hour day_of_month month ?
	yearTimeLimit        = int64(3600 * 24 * 360) // yearTimeLimit 360 day
	createClientLock     sync.Mutex               // createClientLock 互斥锁，避免并发互相覆盖
	sdkLogger            *zap.SugaredLogger       // sdkLogger sdk的日志
	fetchMessageMaxBytes = 2 << 20                // fetchMessageMaxBytes 最大数据可以获取2M
)

// ContractProcessor 核心处理类
type ContractProcessor struct {
	Log                     *zap.SugaredLogger
	ChainClientMp           sync.Map // 便于后续动态增删 //map[string]*sdk.ChainClient //多链支持
	TopicInSubMp            sync.Map // 便于动态管理topic监听 // map[chainId#contractName#topic]struct{}{}
	CloseChan               chan struct{}
	MessageChan             chan *common.ContractEventInfo
	CloseSync               sync.WaitGroup
	SafeClose               chan struct{}
	EventWorkerPool         *ants.Pool
	FibonacciArray          []int64            //
	CronManager             *cron.Cron         // 定时器
	ContractCronManager     *cron.Cron         // 仅仅监控周期性的任务，避免溢出
	EventInProcess          sync.Map           // 用来记录下当前服务实例处理的事件 key：md5（chain id+tx id+request id）
	WatchJobTemplate        string             // 根据配置作监控的模板
	MonitorContractTemplate string             // 根据配置做的监控订阅链上数据消息模板
	SingleBarrier           singleflight.Group // 单飞
}

// ContractFileEntry 合约文件模型
type ContractFileEntry struct {
	ContractName string
	Version      int
	FilePath     string
	ChainId      string
	ChainAlias   string
	NeedOperate  int // 1 : deployed ; 2: upgrade
}

// NewContractProcessor 构造处理类
// @return *ContractProcessor
// @return error
func NewContractProcessor() (*ContractProcessor, error) {
	// 构造日志记录器
	sdkLogger = logger.NewLogger(logger.ModuleSdk, &config.GlobalCFG.LogCFG)
	var ret ContractProcessor
	ret.Log = logger.NewLogger(logger.ModuleOracle, &config.GlobalCFG.LogCFG)
	ret.CloseChan = make(chan struct{})
	ret.SafeClose = make(chan struct{})

	for i := 0; i < len(config.GlobalCFG.SDKS); i++ {
		// 根据配置中的链sdk，连接到每一条链
		tClient, tClientErr := CreateChainClientWithSDKConf(config.GlobalCFG.SDKS[i].ConfigPath)
		if tClientErr != nil {
			ret.Log.Errorf("NewContractProcesser CreateChainClientWithSDKConf chain(%s), error(%s)",
				config.GlobalCFG.SDKS[i].ChainAlias, tClientErr.Error())
			continue
		}
		// 存储一下链别名到链grpc客户端的映射
		ret.ChainClientMp.Store(config.GlobalCFG.SDKS[i].ChainAlias, tClient)
	}

	ret.MessageChan = make(chan *common.ContractEventInfo, config.GlobalCFG.WorkCFG.MessageBuffers) // 配置缓冲区大小
	gRoutine, gErr := ants.NewPool(config.GlobalCFG.WorkCFG.ProcessorCnts, ants.WithPreAlloc(false))
	if gErr != nil {
		return nil, gErr
	}
	ret.EventWorkerPool = gRoutine
	// 根据配置最大重试次数，初始fibonacci数组(效率原因，不实时计算，预先算好)
	ret.FibonacciArray = make([]int64, config.GlobalCFG.WorkCFG.FibonacciLimits)
	for i := 0; i < config.GlobalCFG.WorkCFG.FibonacciLimits; i++ {
		ret.FibonacciArray = append(ret.FibonacciArray, 0)
	}
	fibonacciDupNumber(config.GlobalCFG.WorkCFG.FibonacciLimits-1, ret.FibonacciArray) // 初始化fibonacci数
	// 初始化定时器
	cSec := config.GlobalCFG.WorkCFG.ProcessOvertime % 60
	cSecStr := "*"
	if cSec > 0 {
		cSecStr = fmt.Sprintf("*/%d", cSec)
	}
	cMin := config.GlobalCFG.WorkCFG.ProcessOvertime / 60
	cMinStr := "*"
	if cMin > 0 {
		cMinStr = fmt.Sprintf("*/%d", cMin)
	}
	ret.WatchJobTemplate = fmt.Sprintf("%s %s * * * ?", cSecStr, cMinStr)                                 // 监控事件模板
	ret.MonitorContractTemplate = fmt.Sprintf("@every %dm", config.GlobalCFG.WorkCFG.MonitorContractTime) // 监控合约文件改变模板
	ret.CronManager = cron.New(cron.WithSeconds())                                                        //初始化事件定时器
	ret.ContractCronManager = cron.New(cron.WithSeconds())                                                // 初始化合约表格监控定时器
	go ret.CronManager.Start()                                                                            // 定时器启动
	go ret.ContractCronManager.Start()                                                                    // 监控合约文件周期性
	return &ret, nil
}

// CreateChainClientWithSDKConf create a chain client with sdk config file path
// @param string
// @return *sdk.ChainClient
// @return error
func CreateChainClientWithSDKConf(sdkConfPath string) (*sdk.ChainClient, error) {
	createClientLock.Lock()
	defer createClientLock.Unlock()
	cc, err := sdk.NewChainClient(
		sdk.WithConfPath(sdkConfPath),
		sdk.WithChainClientLogger(sdkLogger),
	)
	if err != nil {
		return nil, err
	}
	// Enable certificate compression
	if cc.GetAuthType() == sdk.PermissionedWithCert {
		err = cc.EnableCertHash()
	}
	if err != nil {
		return nil, err
	}
	return cc, nil
}

// GetEndorsersWithAuthType 验签名
// @param crypto.HashType
// @param sdk.AuthType
// @param *common.Payload
// @param []config.Sign
// @return []*common.EndorsementEntry
// @return error
func GetEndorsersWithAuthType(hashType crypto.HashType,
	authType sdk.AuthType, payload *common.Payload,
	signs []config.Sign) ([]*common.EndorsementEntry, error) {
	var endorsers []*common.EndorsementEntry
	for _, sign := range signs {
		var entry *common.EndorsementEntry
		var err error
		switch authType {
		case sdk.PermissionedWithCert:
			// 签名
			entry, err = sdkutils.MakeEndorserWithPath(sign.UserSignKeyFilePath,
				sign.UserSignCrtFilePath, payload)
			if err != nil {
				return nil, err
			}

		case sdk.PermissionedWithKey:
			// 签名
			entry, err = sdkutils.MakePkEndorserWithPath(sign.UserSignKeyFilePath,
				hashType, sign.OrgId, payload)
			if err != nil {
				return nil, err
			}

		case sdk.Public:
			// 签名
			entry, err = sdkutils.MakePkEndorserWithPath(sign.UserSignKeyFilePath,
				hashType, "", payload)
			if err != nil {
				return nil, err
			}

		default:
			return nil, errors.New("invalid authType")
		}
		endorsers = append(endorsers, entry)
	}

	return endorsers, nil
}

// CheckProposalRequestResp 检查调用链返回的数据是否有问题
// @param *common.TxResponse
// @param bool
// @return error
func CheckProposalRequestResp(resp *common.TxResponse, needContractResult bool) error {
	respInfo := fmt.Sprintf("resp is : %+v", resp)
	if resp.Code != common.TxStatusCode_SUCCESS {
		return errors.New(respInfo)
	}
	if needContractResult && resp.ContractResult == nil {
		return errors.New(respInfo)
	}
	if resp.ContractResult != nil && resp.ContractResult.Code != 0 {
		return errors.New(respInfo)
	}
	return nil
}

// ScanDbContracts 从数据库中按链id查询出所有的已安装合约，chain alias->contract name->contract info
// @return map[string]map[string]models.ContractInfo
// @return error
func ScanDbContracts() (map[string]map[string]models.ContractInfo, error) {
	dbChainContracts := make(map[string]map[string]models.ContractInfo)
	// 从数据库中扫描已经安装的预言机合约
	dbContracts, dbContractsErr := models.LoadContracts()
	if dbContractsErr != nil {
		return dbChainContracts, dbContractsErr
	}
	for _, dbContract := range dbContracts {
		tempContractVersionMp, tempContractVersionMpOk := dbChainContracts[dbContract.ChainAlias]
		if tempContractVersionMpOk {
			tempContractVersionMp[dbContract.ContractName] = dbContract
		} else {
			// 合约名称->合约信息
			contractVersionMp := make(map[string]models.ContractInfo)
			contractVersionMp[dbContract.ContractName] = dbContract
			dbChainContracts[dbContract.ChainAlias] = contractVersionMp
		}
	}
	return dbChainContracts, nil
}

// getSdkConfigByChainAlias 根据链id查询出对应的链配置
// @param string
// @return *config.SDKConfig
// @return bool
func getSdkConfigByChainAlias(chainAlias string) (*config.SDKConfig, bool) {
	for i := 0; i < len(config.GlobalCFG.SDKS); i++ {
		tSdk := config.GlobalCFG.SDKS[i]
		if tSdk.ChainAlias == chainAlias {
			return &tSdk, true
		}
	}
	return nil, false
}
