/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"fmt"
	"testing"
	"time"

	"chainmaker.org/chainmaker/pb-go/v2/common"
)

func TestFibonacciNumber(t *testing.T) {
	retints := make([]int64, 50)
	for i := 0; i < 50; i++ {
		retints = append(retints, 0)
	}
	for i := 0; i < 50; i++ {
		retints[i] = fibonacciNumber(i)
	}
	fmt.Println(retints)
}

func TestFibonacciDupNumber(t *testing.T) {

	retints2 := make([]int64, 50)
	for i := 0; i < 50; i++ {
		retints2 = append(retints2, 0)
	}
	fibonacciDupNumber(49, retints2)
	for i := 0; i < 50; i++ {
		retints2[i] = 30 * retints2[i]
	}
	fmt.Println(retints2)
	timeN := time.Now() //.AddDate(100, 0, 0)
	timeNP := timeN.Add(time.Duration(retints2[30]) * 30 * time.Second)
	fmt.Println(retints2[30:])
	fmt.Println(timeNP)
}

func TestCheckRuntimeParam(t *testing.T) {
	for _, v := range common.RuntimeType_name {
		ok, ind := CheckRuntimeParam(v)
		fmt.Println(ok, ind)
	}
}

func TestMakeContractDeployOrUpgradeKV(t *testing.T) {
	val := makeContractDeployOrUpgradeKV("chain1", "1", "oracle_contract_v1", []byte{'a', '1'})
	fmt.Printf("%+v  ", val)
}

func TestMakeDbContractFromContractEntry(t *testing.T) {
	makeDbContractFromContractEntry(ContractFileEntry{
		ContractName: "oracle_contract_v1",
		Version:      1,
		FilePath:     "../oracle_contract_v1",
		ChainId:      "chain1",
		NeedOperate:  1,
	})
}
