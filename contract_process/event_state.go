/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/alarms"
	"chainmaker-oracle/config"
	"chainmaker-oracle/contract_process/vrf_utils"
	"chainmaker-oracle/fetch_data"
	"chainmaker-oracle/models"
	"errors"
	"fmt"
	"time"

	"chainmaker.org/chainmaker/pb-go/v2/common"
)

const (
	globalJobIsCron = 2 // globalJobIsCron 监控任务类常量
)

// EventV 存储定时任务详情
type EventV struct {
	EntryId int //若为定时任务，则为job id，其余无意义
	State   int // 1 为内存中任务抢到的任务；2为未抢到的定时监控任务；
}

// StateInitProcess 处理刚收到监听的query事件
// @param *models.EventState
// @return error
func (cp *ContractProcessor) StateInitProcess(event *models.EventState) error {
	//返回错误
	fetcher := GetDataFetcher(event, cp)
	queryRet, queryRetErr := fetcher.Fetch(string(event.QueryBody))
	var eventResult *models.EventResult
	if queryRetErr != nil {
		cp.Log.Warnf("stateInitProcess error(%s), "+
			"data(%+v), querybody(%s) ", queryRetErr.Error(),
			event, string(event.QueryBody))
		event.State = models.StateFetchDataFailed
		event.NextFibonacci = 1
		eventResult = &models.EventResult{
			RequestId:   event.RequestId,
			EventResult: []byte(queryRetErr.Error()),
		}
	} else {
		event.State = models.StateFetchDataSuccess
		eventResult = &models.EventResult{
			RequestId:   event.RequestId,
			EventResult: []byte(queryRet),
		}
		if len(queryRet) > fetchMessageMaxBytes {
			event.State = models.StateFetchDataFailed
			eventResult.EventResult = []byte("data length too long")
			cp.Log.Warnf("stateInitProcess long, data(%+v), querybody(%s)",
				event, string(event.QueryBody))
		}
	}
	insertErr := models.InsertEventResult(eventResult)
	if insertErr != nil {
		cp.Log.Warnf("stateInitProcess.InsertEventResult error(%s), event(%+v)",
			insertErr.Error(), event)
		return insertErr //
	}
	updateErr := models.UpdateProcessEvent(event)
	if updateErr != nil {
		cp.Log.Warnf("stateInitProcess  UpdateProcessEvent error(%s), data(%+v)",
			updateErr.Error(), event)

		return updateErr
	}
	updateLockErr := models.UpdateLock(&models.EventLock{
		RequestId: event.RequestId,
	})
	if updateLockErr != nil {
		cp.Log.Warnf("stateInitProcess  UpdateLock error(%s), data(%+v)",
			updateLockErr.Error(), event)
	}
	return nil
}

// StateCallbackFailedProcess 处理回调链失败状态的query事件
// @param *models.EventState
// @param string
// @param string
// @param string
// @return error
func (cp *ContractProcessor) StateCallbackFailedProcess(chainAlias,
	txId, requestId string, event *models.EventState) error {
	//返回错误
	//返回是否中断循环（true为中断循环）
	if event.NextFibonacci >= int64(config.GlobalCFG.WorkCFG.FibonacciLimits) { //超过了fibonacci数列限制
		_ = alarms.GetAlarmSender().SendAlarm(fmt.Sprintf("event(%+v) call chain error succeed %d",
			event, config.GlobalCFG.WorkCFG.FibonacciLimits)) //todo
		event.State = models.StateCallbackFailedFibonacci
		updateErr := models.UpdateProcessEvent(event)
		if updateErr != nil {
			cp.Log.Warnf("stateCallbackFailedProcess UpdateProcessEvent error(%s), data(%+v)",
				updateErr.Error(), event)
		} else {
			_ = models.UpdateLock(&models.EventLock{
				RequestId: requestId,
			})
			cp.RemoveCronJob(chainAlias, txId, requestId)
			return nil //已经修改好了（别的服务改了状态或者自己修改好了
		}
	} else {
		nowTs := time.Now()
		fibonacciSecond := cp.FibonacciArray[event.NextFibonacci] * int64(config.GlobalCFG.WorkCFG.FibonacciTimeunit)
		if fibonacciSecond > yearTimeLimit {
			fibonacciSecond = yearTimeLimit
		}
		cronTime := event.UpdatedAt.Add(time.Second * time.Duration(fibonacciSecond))
		fetchResult, fetchResultErr := models.QueryEventResult(event.RequestId)
		if fetchResultErr != nil {
			cp.Log.Warnf("stateCallbackFailedProcess QueryEventResult err(%s) , event (%+v)",
				fetchResultErr.Error(), event)
			return fetchResultErr
		}
		if nowTs.After(cronTime) {
			//说明已经过期了，可以开始处理再次调用即可
			invokeErr := cp.InvokeContract(event.ChainAlias,
				event.ContractName, invokeMethod,
				makeFetchDataCallbackKvs(fetchResult, event))
			if invokeErr != nil {
				cp.Log.Warnf("stateCallbackFailedProcess InvokeContract err(%s), event (%+v)",
					invokeErr.Error(), event)
				event.State = models.StateCallbackFailed
				event.NextFibonacci = event.NextFibonacci + 1 //调用失败了，更新一下fibonacci数列
			} else {
				event.State = models.StateCallbackSuccess //回调成功
			}
			updateErr := models.UpdateProcessEvent(event)
			if updateErr != nil {
				cp.Log.Warnf("stateCallbackFailedProcess UpdateProcessEvent error(%s), data(%+v)",
					updateErr.Error(), event)
				return updateErr
			}
		} else {
			//加一个定时任务
			cp.RemoveCronJob(event.ChainAlias, event.TxId, event.RequestId)
			hour, minute, second := cronTime.Clock()
			_, month, day := cronTime.Date()
			spec := fmt.Sprintf(cronStandardTemplate, second, minute, hour, day, month)
			job := &WatchCronJobs{
				JobType:    watchTypeFibonacci,
				ChainAlias: event.ChainAlias,
				TxId:       event.TxId,
				RequestId:  event.RequestId,
				Processor:  cp,
			}
			entryId, addJobErr := cp.CronManager.AddJob(spec, job) //
			if addJobErr != nil {
				cp.Log.Errorf("stateCallbackFailedProcess addFunc err, %s", addJobErr.Error())
			}
			job.EntryId = entryId
			cp.EventInProcess.Store(models.ComputeHashKey(chainAlias, txId, requestId), EventV{
				EntryId: int(entryId),
				State:   globalJobIsCron,
			})
			return nil
		}
	}
	return nil
}

// StateFetchDataProcess 处理已经取到数据结果的query事件
// @param *models.EventState
// @return error
func (cp *ContractProcessor) StateFetchDataProcess(event *models.EventState) error {
	//返回错误
	//返回是否中断循环（true为中断循环）
	fetchResult, fetchResultErr := models.QueryEventResult(event.RequestId)
	if fetchResultErr != nil {
		cp.Log.Warnf("stateFetchDataProcess Success/Failed QueryEventResult err(%s) , event (%+v)",
			fetchResultErr.Error(), event)
		return fetchResultErr
	}
	invokeErr := cp.InvokeContract(event.ChainAlias,
		event.ContractName, invokeMethod,
		makeFetchDataCallbackKvs(fetchResult, event))
	if invokeErr != nil {
		cp.Log.Warnf("stateFetchDataProcess Success/Failed InvokeContract err(%s), event (%+v)",
			invokeErr.Error(), event)
		event.State = models.StateCallbackFailed
		event.NextFibonacci = event.NextFibonacci + 1 //调用失败了，更新一下fibonacci数列
	} else {
		event.State = models.StateCallbackSuccess //回调成功
	}
	updateErr := models.UpdateProcessEvent(event)
	if updateErr != nil {
		cp.Log.Warnf("stateFetchDataProcess Success/Failed UpdateProcessEvent error(%s), data(%+v)",
			updateErr.Error(), event)
		return updateErr // ?
	}
	updateLockErr := models.UpdateLock(&models.EventLock{
		RequestId: event.RequestId,
	}) //更新一把锁
	if updateLockErr != nil {
		cp.Log.Warnf("stateFetchDataProcess  UpdateLock error(%s), data(%+v) ",
			updateLockErr.Error(), event)
	}
	return nil
}

// Run 状态机入口
// @param string
// @param string
// @param string
// @return error
func (cp *ContractProcessor) Run(chainAlias, txId, requestId string) error {
	needBreak := false
	for !needBreak {
		// 查询一下事件状态
		event, dbEventStateErr := models.QueryProcessEvent(requestId)
		if dbEventStateErr != nil {
			cp.Log.Errorf("Run QueryProcessEvent err , %s  ", dbEventStateErr.Error())
			// 加一个定时任务，监控
			cp.addWatchUnFinishedJob(chainAlias, requestId, txId)
			return dbEventStateErr
		}
		if event == nil { // 理论上来说走不到这个分支，如果走到这个分支，说明数据丢失了
			cp.RemoveCronJob(chainAlias, txId, requestId)
			break
		}
		switch event.State {
		case models.StateResultOnChainSuccess:
			// 已经上链成功
			cp.RemoveCronJob(chainAlias, txId, requestId)
			needBreak = true
		case models.StateCallbackFailedFibonacci:
			// fibonacci重试超过了配置的次数
			cp.RemoveCronJob(chainAlias, txId, requestId)
			needBreak = true
		case models.StateCallbackSuccess:
			// 回调链成功
			cp.RemoveCronJob(chainAlias, txId, requestId)
			needBreak = true
		case models.StateInit:
			// 刚从链上监听到一次查询事件
			initErr := cp.StateInitProcess(event)
			if initErr != nil {
				// 处理遇到了问题，将任务加到定时器中，稍后重试
				cp.addWatchUnFinishedJob(event.ChainAlias, event.RequestId, event.TxId)
				return initErr
			}
		case models.StateCallbackFailed:
			// 调用链失败
			callbackErr := cp.StateCallbackFailedProcess(chainAlias, txId, requestId, event)
			if callbackErr != nil {
				// 处理遇到了问题，将任务加到定时器中，稍后重试
				cp.addWatchUnFinishedJob(event.ChainAlias, event.RequestId, event.TxId)
				return callbackErr
			}
		case models.StateFetchDataSuccess, models.StateFetchDataFailed:
			// 获取数据成功、失败
			fetchErr := cp.StateFetchDataProcess(event)
			if fetchErr != nil {
				// 处理遇到了问题，将任务加到定时器中，稍后重试
				cp.addWatchUnFinishedJob(event.ChainAlias, event.RequestId, event.TxId)
				return fetchErr
			}
		}

	}
	return nil
}

// addWatchUnFinishedJob 未完成任务添加到定时监控
// @param string
// @param string
// @param string
func (cp *ContractProcessor) addWatchUnFinishedJob(chainAlias, requestId, txId string) {
	// 根据链别名+事务id+请求id，计算一下hash
	hashKey := models.ComputeHashKey(chainAlias, txId, requestId)
	// 从内存中查一下看看有没有定时任务监控
	_, exist := cp.EventInProcess.Load(hashKey)
	if exist {
		return
	}
	wJob := &WatchCronJobs{
		JobType:    watchTypeCron,
		ChainAlias: chainAlias,
		RequestId:  requestId,
		TxId:       txId,
		Processor:  cp,
	}
	// 定时器中加一个任务
	entryId, _ := cp.CronManager.AddJob(cp.WatchJobTemplate, wJob)
	wJob.EntryId = entryId
	// 内存中记录一下定时任务
	cp.EventInProcess.Store(hashKey, EventV{
		EntryId: int(entryId),
		State:   globalJobIsCron,
	})
}

// makeFetchDataCallbackKvs 构造回调链参数
// @param *models.EventResult
// @param *models.EventState
// @return []*common.KeyValuePair
func makeFetchDataCallbackKvs(dbResult *models.EventResult, eventState *models.EventState) []*common.KeyValuePair {
	if dbResult == nil {
		dbResult = &models.EventResult{} //防空指针
	}
	var originalMethod string
	switch eventState.QueryType {
	case models.QueryTypeHttp:
		originalMethod = models.QueryHttpMethod
	case models.QueryTypeMysql:
		originalMethod = models.QueryMysqlMethod
	case models.QueryTypeGetVRF:
		originalMethod = models.QueryVRFMethod
	case models.QueryTypeVerifyVRF:
		originalMethod = models.VerifyMethod
	case models.QueryTypeCrossChain:
		originalMethod = models.QueryCrossChainMethod
	}
	if eventState.State == models.StateFetchDataSuccess {
		return []*common.KeyValuePair{
			{
				Key:   "original_method",
				Value: []byte(originalMethod),
			},
			{
				Key:   "method",
				Value: []byte(callbackMethod),
			},
			{
				Key:   "requestId",
				Value: []byte(eventState.RequestId), //这个值与请求的requestId是一致的
			},
			{
				Key:   "txId",
				Value: []byte(eventState.TxId), //这个值与请求的tx id是一样的
			},
			{
				Key:   "code",
				Value: []byte("202"),
			},
			{
				Key:   "chainAlias",
				Value: []byte(eventState.ChainAlias),
			},
			{
				Key:   "message",
				Value: []byte("success"),
			},
			{
				Key:   "result",
				Value: dbResult.EventResult,
			},
		}
	}
	return []*common.KeyValuePair{
		{
			Key:   "original_method",
			Value: []byte(originalMethod),
		},
		{
			Key:   "method",
			Value: []byte(callbackMethod),
		},
		{
			Key:   "requestId",
			Value: []byte(eventState.RequestId), //这个值与请求的requestId是一致的
		},
		{
			Key:   "txId",
			Value: []byte(eventState.TxId), //这个值与请求的tx id是一样的
		},
		{
			Key:   "code",
			Value: []byte("400"), //报错了
		},
		{
			Key:   "chainAlias",
			Value: []byte(eventState.ChainAlias),
		},
		{
			Key:   "message",
			Value: dbResult.EventResult, //错误信息
		},
	}
}

// GetDataFetcher 根据传入的参数来获取具体查数据的工作类
// @param *models.EventState
// @param *ContractProcessor
// @return fetch_data.Fetcher
func GetDataFetcher(event *models.EventState, cp *ContractProcessor) fetch_data.Fetcher {
	if event.QueryType == models.QueryTypeHttp {
		return &fetch_data.FetchHttp{}
	} else if event.QueryType == models.QueryTypeMysql {
		return &fetch_data.FetchDatabase{}
	} else if event.QueryType == models.QueryTypeGetVRF {
		return &vrf_utils.VRFGetter{}
	} else if event.QueryType == models.QueryTypeVerifyVRF {
		return &vrf_utils.VRFVerifier{}
	} else if event.QueryType == models.QueryTypeCrossChain {
		return cp
	}
	return &DummyFetcher{}
}

// DummyFetcher Fetch接口的空实现
type DummyFetcher struct {
}

// Fetch 接口空实现
// @param string
// @return []byte
// @return error
func (fetcher *DummyFetcher) Fetch(fetchCfg string) ([]byte, error) {
	return []byte(""), nil
}

// ErrorFetcher 错误实现，便于测试
type ErrorFetcher struct {
}

// Fetch 接口实现
// @param string
// @return []byte
// @return error
func (fetcher *ErrorFetcher) Fetch(fetchCfg string) ([]byte, error) {
	return nil, errors.New("test error fetcher")
}
