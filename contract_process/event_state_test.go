/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/fetch_data"
	"chainmaker-oracle/models"
	"errors"
	"os"
	"reflect"
	"testing"
	"time"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	cfg "chainmaker.org/chainmaker/pb-go/v2/config"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	. "github.com/agiledragon/gomonkey"
	"github.com/robfig/cron/v3"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetDataFetcher(t *testing.T) {
	Convey("GetDataFetcher", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		Convey("GetDataFetcher.http", func() {
			ret := GetDataFetcher(&models.EventState{
				QueryType: models.QueryTypeHttp,
			}, processor)
			So(ret, ShouldNotBeNil)
		})
		Convey("GetDataFetcher.mysql", func() {
			ret := GetDataFetcher(&models.EventState{
				QueryType: models.QueryTypeMysql,
			}, processor)
			So(ret, ShouldNotBeNil)
		})
		Convey("GetDataFetcher.vrf", func() {
			ret := GetDataFetcher(&models.EventState{
				QueryType: models.QueryTypeGetVRF,
			}, processor)
			So(ret, ShouldNotBeNil)
		})
		Convey("GetDataFetcher.verify", func() {
			ret := GetDataFetcher(&models.EventState{
				QueryType: models.QueryTypeVerifyVRF,
			}, processor)
			So(ret, ShouldNotBeNil)
		})
		Convey("GetDataFetcher.cross", func() {
			ret := GetDataFetcher(&models.EventState{
				QueryType: models.QueryTypeCrossChain,
			}, processor)
			So(ret, ShouldNotBeNil)
		})
	})
}

func TestMakeFetchDataCallbackKvs(t *testing.T) {
	Convey("success", t, func() {
		dbResult := models.EventResult{
			RequestId:   "ajfaij23r2r3",
			CreatedAt:   time.Now(),
			EventResult: []byte("success"),
		}
		state := models.EventState{
			ChainAlias: "chain1",
			RequestId:  dbResult.RequestId,
			State:      models.StateFetchDataSuccess,
		}
		Convey("fetchDataSuccess", func() {
			ret := makeFetchDataCallbackKvs(&dbResult, &state)
			So(len(ret), ShouldBeGreaterThan, 0)
		})
		state.State = models.StateFetchDataFailed
		Convey("fetchDataFailed", func() {
			ret := makeFetchDataCallbackKvs(&dbResult, &state)
			So(len(ret), ShouldBeGreaterThan, 0)
		})
	})
}

func TestAddWatchUnFinishedJob(t *testing.T) {
	Convey("addWatchUnfinishedJob", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		patchjob := ApplyMethod(reflect.TypeOf(processor.CronManager), "AddJob", func(*cron.Cron, string, cron.Job) (cron.EntryID, error) {
			return cron.EntryID(1), nil
		})
		defer patchjob.Reset()
		processor.addWatchUnFinishedJob("chain1_alias", "xfjai24r2xx", "il23")
	})
}

func TestRun(t *testing.T) {
	Convey("Run", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		patchjob := ApplyMethod(reflect.TypeOf(processor.CronManager), "AddJob", func(*cron.Cron, string, cron.Job) (cron.EntryID, error) {
			return cron.EntryID(1), nil
		})
		defer patchjob.Reset()
		Convey("run.init", func() {
			patch2 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
				return &models.EventState{
					State:      models.StateInit,
					ChainAlias: "chain1_alias",
					RequestId:  "sfjai02802jkjdfmk000afd",
					TxId:       "afj233fj23javmb9",
				}, nil
			})
			defer patch2.Reset()

			patch3 := ApplyMethod(reflect.TypeOf(processor), "StateInitProcess", func(*ContractProcessor, *models.EventState) error {
				return errors.New("break")
			})
			defer patch3.Reset()
			err := processor.Run("chain1_alias", "afj233fj23javmb9", "sfjai02802jkjdfmk000afd")
			So(err, ShouldNotBeNil)

		})
		Convey("run.queryerr", func() {
			patch4 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
				return nil, errors.New("mysql db close")
			})
			defer patch4.Reset()

			err := processor.Run("chain1_alias", "afj233fj23javmb9", "sfjai02802jkjdfmk000afd")
			So(err, ShouldBeError)
		})

		Convey("run.StateCallbackFailed", func() {
			patch5 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
				return &models.EventState{
					State:      models.StateCallbackFailed,
					ChainAlias: "chain1_alias",
					RequestId:  "sfjai02802jkjdfmk000afd",
					TxId:       "afj233fj23javmb9",
				}, nil
			})
			defer patch5.Reset()

			patch6 := ApplyMethod(reflect.TypeOf(processor), "StateCallbackFailedProcess", func(cp *ContractProcessor, chainId,
				txId, requestId string, event *models.EventState) error {
				return errors.New("break")
			})
			defer patch6.Reset()
			err := processor.Run("chain1_alias", "afj233fj23javmb9", "sfjai02802jkjdfmk000afd")
			So(err, ShouldBeError)
		})

		Convey("run.StateFetchDataSuccess", func() {
			patch7 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
				return &models.EventState{
					State:      models.StateFetchDataSuccess,
					ChainAlias: "chain1_alias",
					RequestId:  "sfjai02802jkjdfmk000afd",
					TxId:       "afj233fj23javmb9",
				}, nil
			})
			defer patch7.Reset()

			patch8 := ApplyMethod(reflect.TypeOf(processor), "StateFetchDataProcess", func(*ContractProcessor, *models.EventState) error {
				return errors.New("break")
			})
			defer patch8.Reset()
			err := processor.Run("chain1_alias", "afj233fj23javmb9", "sfjai02802jkjdfmk000afd")
			So(err, ShouldBeError)
		})

		Convey("run.StateResultOnChainSuccess", func() {
			patch9 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
				return &models.EventState{
					State:      models.StateResultOnChainSuccess,
					ChainAlias: "chain1_alias",
					RequestId:  "sfjai02802jkjdfmk000afd",
					TxId:       "afj233fj23javmb9",
				}, nil
			})
			defer patch9.Reset()
			err := processor.Run("chain1_alias", "afj233fj23javmb9", "sfjai02802jkjdfmk000afd")
			So(err, ShouldBeNil)
		})

		Convey("run.StateCallbackFailedFibnoacci", func() {
			patch10 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
				return &models.EventState{
					State:      models.StateCallbackFailedFibonacci,
					ChainAlias: "chain1_alias",
					RequestId:  "sfjai02802jkjdfmk000afd",
					TxId:       "afj233fj23javmb9",
				}, nil
			})
			defer patch10.Reset()
			err := processor.Run("chain1_alias", "afj233fj23javmb9", "sfjai02802jkjdfmk000afd")
			So(err, ShouldBeNil)
		})

		Convey("run.StateCallbackSuccess", func() {
			patch11 := ApplyFunc(models.QueryProcessEvent, func(string) (*models.EventState, error) {
				return &models.EventState{
					State:      models.StateCallbackSuccess,
					ChainAlias: "chain1_alias",
					RequestId:  "sfjai02802jkjdfmk000afd",
					TxId:       "afj233fj23javmb9",
				}, nil
			})
			defer patch11.Reset()
			err := processor.Run("chain1_alias", "afj233fj23javmb9", "sfjai02802jkjdfmk000afd")
			So(err, ShouldBeNil)
		})

	})
}

func TestStateInitProcess(t *testing.T) {
	Convey("stateInitProcess", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		patchjob := ApplyMethod(reflect.TypeOf(processor.CronManager), "AddJob", func(*cron.Cron, string, cron.Job) (cron.EntryID, error) {
			return cron.EntryID(1), nil
		})
		defer patchjob.Reset()
		Convey("query.success", func() {
			patch2 := ApplyFunc(GetDataFetcher, func(event *models.EventState, cp *ContractProcessor) fetch_data.Fetcher {
				return &DummyFetcher{}
			})
			defer patch2.Reset()
			patch3 := ApplyFunc(models.InsertEventResult, func(*models.EventResult) error {
				return nil
			})
			defer patch3.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return nil
			})
			defer patch5.Reset()
			err := processor.StateInitProcess(&models.EventState{
				ChainAlias: "chain1_alias",
				RequestId:  "sfjai02802jkjdfmk000afd",
				TxId:       "afj233fj23javmb9",
			})
			So(err, ShouldBeNil)
		})

		Convey("query.error", func() {
			patch2 := ApplyFunc(GetDataFetcher, func(event *models.EventState, cp *ContractProcessor) fetch_data.Fetcher {
				return &ErrorFetcher{}
			})
			defer patch2.Reset()
			patch3 := ApplyFunc(models.InsertEventResult, func(*models.EventResult) error {
				return nil
			})
			defer patch3.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return nil
			})
			defer patch5.Reset()
			err := processor.StateInitProcess(&models.EventState{
				ChainAlias: "chain1_alias",
				RequestId:  "sfjai02802jkjdfmk000afd",
				TxId:       "afj233fj23javmb9",
			})
			So(err, ShouldBeNil)
		})

		Convey("query.InsertEventResult.error", func() {
			patch2 := ApplyFunc(GetDataFetcher, func(event *models.EventState, cp *ContractProcessor) fetch_data.Fetcher {
				return &DummyFetcher{}
			})
			defer patch2.Reset()
			patch3 := ApplyFunc(models.InsertEventResult, func(*models.EventResult) error {
				return errors.New("mysql db close")
			})
			defer patch3.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return nil
			})
			defer patch5.Reset()
			err := processor.StateInitProcess(&models.EventState{
				ChainAlias: "chain1_alias",
				RequestId:  "sfjai02802jkjdfmk000afd",
				TxId:       "afj233fj23javmb9",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("query.UpdateProcessEvent.error", func() {
			patch2 := ApplyFunc(GetDataFetcher, func(event *models.EventState, cp *ContractProcessor) fetch_data.Fetcher {
				return &DummyFetcher{}
			})
			defer patch2.Reset()
			patch3 := ApplyFunc(models.InsertEventResult, func(*models.EventResult) error {
				return nil
			})
			defer patch3.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return errors.New("mysql db close")
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return nil
			})
			defer patch5.Reset()
			err := processor.StateInitProcess(&models.EventState{
				ChainAlias: "chain1_alias",
				RequestId:  "sfjai02802jkjdfmk000afd",
				TxId:       "afj233fj23javmb9",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("query.UpdateLock.error", func() {
			patch2 := ApplyFunc(GetDataFetcher, func(event *models.EventState, cp *ContractProcessor) fetch_data.Fetcher {
				return &DummyFetcher{}
			})
			defer patch2.Reset()
			patch3 := ApplyFunc(models.InsertEventResult, func(*models.EventResult) error {
				return nil
			})
			defer patch3.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return errors.New("mysql db close")
			})
			defer patch5.Reset()
			err := processor.StateInitProcess(&models.EventState{
				ChainAlias: "chain1_alias",
				RequestId:  "sfjai02802jkjdfmk000afd",
				TxId:       "afj233fj23javmb9",
			})
			So(err, ShouldBeNil)
		})

	})
}

func TestStateFetchDataProcess(t *testing.T) {

	Convey("StateFetchDataProcess", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		patchjob := ApplyMethod(reflect.TypeOf(processor.CronManager), "AddJob", func(*cron.Cron, string, cron.Job) (cron.EntryID, error) {
			return cron.EntryID(1), nil
		})
		defer patchjob.Reset()
		Convey("callback.success", func() {
			patch2 := ApplyFunc(models.QueryEventResult, func(string) (*models.EventResult, error) {
				return &models.EventResult{
					EventResult: []byte("success"),
					CreatedAt:   time.Now(),
					RequestId:   "sfjai02802jkjdfmk000afd",
				}, nil
			})
			defer patch2.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return nil
			})
			defer patch5.Reset()
			ApplyMethod(reflect.TypeOf(processor), "InvokeContract", func(*ContractProcessor, string, string, string, []*common.KeyValuePair) error {
				return nil
			})
			err := processor.StateFetchDataProcess(&models.EventState{
				State:      models.StateCallbackFailedFibonacci,
				ChainAlias: "chain1_alias",
				RequestId:  "sfjai02802jkjdfmk000afd",
				TxId:       "afj233fj23javmb9",
			})
			So(err, ShouldBeNil)

		})
		Convey("callback.failed", func() {
			patch2 := ApplyFunc(models.QueryEventResult, func(string) (*models.EventResult, error) {
				return &models.EventResult{
					EventResult: []byte("success"),
					CreatedAt:   time.Now(),
					RequestId:   "sfjai02802jkjdfmk000afd",
				}, nil
			})
			defer patch2.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return nil
			})
			defer patch5.Reset()
			ApplyMethod(reflect.TypeOf(processor), "InvokeContract", func(*ContractProcessor, string, string, string, []*common.KeyValuePair) error {
				return errors.New("invoke contract error ")
			})
			err := processor.StateFetchDataProcess(&models.EventState{
				State:      models.StateCallbackFailedFibonacci,
				ChainAlias: "chain1_alias",
				RequestId:  "sfjai02802jkjdfmk000afd",
				TxId:       "afj233fj23javmb9",
			})
			So(err, ShouldBeNil)
		})

		Convey("callback.success.UpdateProcessEvent.err", func() {
			patch2 := ApplyFunc(models.QueryEventResult, func(string) (*models.EventResult, error) {
				return &models.EventResult{
					EventResult: []byte("success"),
					CreatedAt:   time.Now(),
					RequestId:   "sfjai02802jkjdfmk000afd",
				}, nil
			})
			defer patch2.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return errors.New("mysql db closed")
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return nil
			})
			defer patch5.Reset()
			ApplyMethod(reflect.TypeOf(processor), "InvokeContract", func(*ContractProcessor, string, string, string, []*common.KeyValuePair) error {
				return nil
			})
			err := processor.StateFetchDataProcess(&models.EventState{
				State:      models.StateCallbackFailedFibonacci,
				ChainAlias: "chain1_alias",
				RequestId:  "sfjai02802jkjdfmk000afd",
				TxId:       "afj233fj23javmb9",
			})
			So(err, ShouldNotBeNil)
		})

		Convey("callback.success.UpdateLock.err", func() {
			patch2 := ApplyFunc(models.QueryEventResult, func(string) (*models.EventResult, error) {
				return &models.EventResult{
					EventResult: []byte("success"),
					CreatedAt:   time.Now(),
					RequestId:   "sfjai02802jkjdfmk000afd",
				}, nil
			})
			defer patch2.Reset()
			patch4 := ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			defer patch4.Reset()
			patch5 := ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return errors.New("mysql db close")
			})
			defer patch5.Reset()
			ApplyMethod(reflect.TypeOf(processor), "InvokeContract", func(*ContractProcessor, string, string, string, []*common.KeyValuePair) error {
				return nil
			})
			err := processor.StateFetchDataProcess(&models.EventState{
				State:         models.StateCallbackFailedFibonacci,
				ChainAlias:    "chain1_alias",
				RequestId:     "sfjai02802jkjdfmk000afd",
				TxId:          "afj233fj23javmb9",
				NextFibonacci: int64(config.GlobalCFG.WorkCFG.FibonacciLimits) + 1,
			})
			So(err, ShouldBeNil)
		})
	})

}

func TestStateCallbackFailedProcess(t *testing.T) {

	Convey("StateFetchDataProcess", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		patchjob := ApplyMethod(reflect.TypeOf(processor.CronManager), "AddJob", func(*cron.Cron, string, cron.Job) (cron.EntryID, error) {
			return cron.EntryID(1), nil
		})
		defer patchjob.Reset()
		Convey("fibonacci.succeed", func() {
			ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			ApplyFunc(models.UpdateLock, func(*models.EventLock) error {
				return nil
			})
			err := processor.StateCallbackFailedProcess("chain1_alias",
				"afj233fj23javmb9", "sfjai02802jkjdfmk000afd",
				&models.EventState{
					State:         models.StateCallbackFailedFibonacci,
					ChainAlias:    "chain1_alias",
					RequestId:     "sfjai02802jkjdfmk000afd",
					TxId:          "afj233fj23javmb9",
					NextFibonacci: int64(config.GlobalCFG.WorkCFG.FibonacciLimits) + 1,
				})
			So(err, ShouldBeNil)
		})

		Convey("fibonacci.invoke", func() {
			ApplyFunc(models.QueryEventResult, func(string) (*models.EventResult, error) {
				return &models.EventResult{}, nil
			})
			ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			ApplyMethod(reflect.TypeOf(processor), "InvokeContract", func(*ContractProcessor, string, string, string, []*common.KeyValuePair) error {
				return nil
			})
			err := processor.StateCallbackFailedProcess("chain1_alias",
				"afj233fj23javmb9", "sfjai02802jkjdfmk000afd",
				&models.EventState{
					State:         models.StateCallbackFailedFibonacci,
					ChainAlias:    "chain1_alias",
					RequestId:     "sfjai02802jkjdfmk000afd",
					TxId:          "afj233fj23javmb9",
					NextFibonacci: 1,
					UpdatedAt:     time.Now().Add(-1 * time.Hour),
				})
			So(err, ShouldBeNil)
		})

		Convey("fibonacci.addjob", func() {
			ApplyFunc(models.QueryEventResult, func(string) (*models.EventResult, error) {
				return &models.EventResult{}, nil
			})
			ApplyFunc(models.UpdateProcessEvent, func(*models.EventState) error {
				return nil
			})
			ApplyMethod(reflect.TypeOf(processor), "InvokeContract", func(*ContractProcessor, string, string, string, []*common.KeyValuePair) error {
				return nil
			})
			err := processor.StateCallbackFailedProcess("chain1_alias",
				"afj233fj23javmb9", "sfjai02802jkjdfmk000afd",
				&models.EventState{
					State:         models.StateCallbackFailedFibonacci,
					ChainAlias:    "chain1_alias",
					RequestId:     "sfjai02802jkjdfmk000afd",
					TxId:          "afj233fj23javmb9",
					NextFibonacci: 5,
					UpdatedAt:     time.Now().Add(-5 * time.Second),
				})
			So(err, ShouldBeNil)
		})
	})

}
