/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/contract_process/vrf_utils"
	"chainmaker-oracle/models"
	"context"
	"fmt"
	"os"
	"reflect"
	"testing"
	"time"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	cfg "chainmaker.org/chainmaker/pb-go/v2/config"
	sdk "chainmaker.org/chainmaker/sdk-go/v2"
	. "github.com/agiledragon/gomonkey"
	. "github.com/smartystreets/goconvey/convey"
)

func TestGetChainClient(t *testing.T) {
	Convey("GetChainClient", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		clientP := &sdk.ChainClient{}
		defer patch.Reset()
		patch1 := ApplyMethod(reflect.TypeOf(clientP),
			"GetChainConfig", func(*sdk.ChainClient) (*cfg.ChainConfig, error) {
				return &cfg.ChainConfig{}, nil
			})
		defer patch1.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		client, clientErr := processor.GetChainClient("chain1_alias")
		if clientErr != nil {
			return
		}
		fmt.Printf("client: (%+v) ", client)
	})

}

func TestStop(t *testing.T) {
	Convey("Stop", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		go func() {
			time.Sleep(2 * time.Second)
			close(processor.SafeClose)
		}()
		processor.Stop()
	})
}

func TestWatchSubscribeClose(t *testing.T) {
	Convey("WatchSubscribeClose", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		go func() {
			time.Sleep(2 * time.Second)
			close(processor.CloseChan)
			time.Sleep(2 * time.Second)
		}()
		processor.WatchSubscribeClose()
	})
}

func TestConsumeEvents(t *testing.T) {
	Convey("ConsumeEvents", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		go func() {
			time.Sleep(2 * time.Second)
			close(processor.MessageChan)
			time.Sleep(2 * time.Second)
			<-processor.SafeClose
		}()
		processor.ConsumeEvents()
	})
}

// func TestSubscribeContractEvents(t *testing.T) {
// 	Convey("SubscribeContractEvents", t, func() {
// 		config.ReadConfigFile("../config_files/smart_oracle.yml")
// 		os.Chdir("../")
// 		//在这里把他mock住
// 		patch := ApplyFunc(sdk.NewChainClient,
// 			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
// 				return &sdk.ChainClient{}, nil
// 			})
// 		defer patch.Reset()
// 		processor, err := NewContractProcesser()
// 		if err != nil {
// 			return
// 		}
// 		patch1 := ApplyFunc(ScanDbContracts,
// 			func() (map[string]map[string]models.ContractInfo, error) {
// 				mp := make(map[string]models.ContractInfo)
// 				mp["oracle_contract_v1"] = models.ContractInfo{}
// 				retmp := make(map[string]map[string]models.ContractInfo)
// 				retmp["chain1_alias"] = mp
// 				return retmp, nil
// 			})
// 		defer patch1.Reset()
// 		patch2 := ApplyMethod(reflect.TypeOf(processor),
// 			"SubscribeContracts", func(cp *ContractProcesor, chainId,
// 				contractName, topic string, start, end int64) error {
// 				return nil
// 			})
// 		defer patch2.Reset()
// 		patch3 := ApplyMethod(reflect.TypeOf(processor),
// 			"GetChainClient", func(*ContractProcesor, string) (*sdk.ChainClient, error) {
// 				return &sdk.ChainClient{}, nil
// 			})
// 		defer patch3.Reset()
// 		processor.SubscribeContractEvents()

// 	})
// }

func TestUpdateContractAndUpdateDB(t *testing.T) {
	Convey("UpdateContractAndUpdateDB", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		Convey("success", func() {
			patch1 := ApplyMethod(reflect.TypeOf(processor),
				"UpdateContract", func(thip *ContractProcessor,
					chainId, contractName,
					version, byteCodeStringOrFilePath string,
					runtime common.RuntimeType) error {
					return nil
				})
			defer patch1.Reset()
			patch2 := ApplyFunc(models.UpGradeContractInfo,
				func(chainId, contractName string, contractVersion int) error {
					return nil
				})
			defer patch2.Reset()
			_, err := processor.UpdateContractAndUpdateDB("oracle_contract_v1",
				"../contract_files/chain1/oracle_contract_v1/1/oracle_contract_v1.7z",
				"chain1_alias", 1, 1)
			So(err, ShouldBeNil)
		})
	})
}

func TestDeployAndUpdateDB(t *testing.T) {
	Convey("DeployAndUpdateDB", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		Convey("success", func() {
			patch1 := ApplyMethod(reflect.TypeOf(processor),
				"DeployContract", func(thip *ContractProcessor,
					chainId, contractName,
					version, byteCodeStringOrFilePath string,
					runtime common.RuntimeType) error {
					return nil
				})
			defer patch1.Reset()
			patch2 := ApplyFunc(models.InsertContract,
				func(*models.ContractInfo) error {
					return nil
				})
			defer patch2.Reset()
			patch3 := ApplyMethod(reflect.TypeOf(processor),
				"GetChainClient", func(*ContractProcessor, string) (*sdk.ChainClient, error) {
					return &sdk.ChainClient{}, nil
				})
			defer patch3.Reset()
			_, err := processor.DeployAndUpdateDB("oracle_contract_v1",
				"../contract_files/chain1/oracle_contract_v1/1/oracle_contract_v1.7z",
				"chain1_alias", "chain1", 1, 1)
			So(err, ShouldBeNil)
		})
	})
}

func TestSubscribeContracts(t *testing.T) {
	Convey("SubscribeContracts", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		Convey("success", func() {
			patch1 := ApplyMethod(reflect.TypeOf(processor),
				"GetChainClient", func(thip *ContractProcessor,
					chainId string) (*sdk.ChainClient, error) {
					return &sdk.ChainClient{}, nil
				})
			defer patch1.Reset()
			clientP := &sdk.ChainClient{}
			patch2 := ApplyMethod(reflect.TypeOf(clientP),
				"SubscribeContractEvent",
				func(cp *sdk.ChainClient,
					ctx context.Context, startBlock, endBlock int64,
					contractName,
					topic string) (<-chan interface{}, error) {
					c := make(chan interface{})
					go func() {
						time.Sleep(2 * time.Second)
						c <- &common.ContractEventInfo{
							BlockHeight:     1,
							ChainId:         "chain1_alias",
							Topic:           "oracle_query",
							TxId:            "xweji27323",
							EventIndex:      1,
							ContractName:    "oracle_contract_v1",
							ContractVersion: "1",
							EventData: []string{"1", "chain1",
								"sab23239#dafj23", "xweji27323", "1", "{}"},
						}
						time.Sleep(2 * time.Second)
						close(c)
					}()
					return c, nil

				})
			defer patch2.Reset()
			err := processor.SubscribeContracts("chain1_alias", "oracle_contract_v1",
				"oracle_query",
				1, 1)
			So(err, ShouldNotBeNil)
		})
	})
}

func TestInvokeContract(t *testing.T) {
	Convey("InvokeContract", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		Convey("success", func() {

			clientP := &sdk.ChainClient{}
			patch1 := ApplyMethod(reflect.TypeOf(processor),
				"GetChainClient", func(thip *ContractProcessor,
					chainId string) (*sdk.ChainClient, error) {
					return &sdk.ChainClient{}, nil
				})
			defer patch1.Reset()

			patch2 := ApplyMethod(reflect.TypeOf(clientP),
				"InvokeContract",
				func(cp *sdk.ChainClient,
					contractName, method, txId string,
					kvs []*common.KeyValuePair, timeout int64,
					withSyncResult bool) (*common.TxResponse, error) {
					return &common.TxResponse{
						Code:    0,
						Message: "",
						TxId:    "2323dfaaf",
						ContractResult: &common.ContractResult{
							Code:    0,
							Result:  []byte("success"),
							Message: "",
							GasUsed: 12,
						},
					}, nil
				})
			defer patch2.Reset()
			err := processor.InvokeContract("chain1_alias",
				"oracle_contract_v1", "invoke_contract",
				[]*common.KeyValuePair{
					{
						Key:   "method",
						Value: []byte("callback"),
					},
					{
						Key:   "requestId",
						Value: []byte("1"), //这个值与请求的requestId是一致的
					},
					{
						Key:   "txId",
						Value: []byte("4e1d13be26b149908ec80dd0966fce2d3e7a03bff91a4b0f879639c6168f9f04"), //这个值与请求的txid是一样的
					},
					{
						Key:   "code",
						Value: []byte("0"),
					},
					{
						Key:   "chainAlias",
						Value: []byte("chain1_alias"),
					},
					{
						Key:   "message",
						Value: []byte("success"),
					},
					{
						Key:   "result",
						Value: []byte("query-http-success"),
					},
				})
			So(err, ShouldBeNil)
		})
	})

}

func TestQueryContract(t *testing.T) {
	Convey("QueryContract", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		Convey("success", func() {

			clientP := &sdk.ChainClient{}
			patch1 := ApplyMethod(reflect.TypeOf(processor),
				"GetChainClient", func(thip *ContractProcessor,
					chainId string) (*sdk.ChainClient, error) {
					return &sdk.ChainClient{}, nil
				})
			defer patch1.Reset()

			patch2 := ApplyMethod(reflect.TypeOf(clientP),
				"QueryContract",
				func(cp *sdk.ChainClient,
					contractName, method string,
					kvs []*common.KeyValuePair, timeout int64,
				) (*common.TxResponse, error) {
					return &common.TxResponse{
						Code:    0,
						Message: "",
						TxId:    "2323dfaaf",
						ContractResult: &common.ContractResult{
							Code:    0,
							Result:  []byte("success"),
							Message: "",
							GasUsed: 12,
						},
					}, nil
				})
			defer patch2.Reset()
			_, err := processor.QueryContract("chain1_alias",
				"oracle_contract_v1", "invoke_contract",
				[]*common.KeyValuePair{
					{
						Key:   "method",
						Value: []byte("callback"),
					},
					{
						Key:   "requestId",
						Value: []byte("1"), //这个值与请求的requestId是一致的
					},
					{
						Key:   "txId",
						Value: []byte("4e1d13be26b149908ec80dd0966fce2d3e7a03bff91a4b0f879639c6168f9f04"), //这个值与请求的txid是一样的
					},
					{
						Key:   "code",
						Value: []byte("0"),
					},
					{
						Key:   "chainId",
						Value: []byte("chain1_alias"),
					},
					{
						Key:   "message",
						Value: []byte("success"),
					},
					{
						Key:   "result",
						Value: []byte("query-http-success"),
					},
				})
			So(err, ShouldBeNil)
		})
	})
}

func TestUpdateContract(t *testing.T) {
	Convey("QueryContract", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		vrf_utils.GlobalECVRF = &vrf_utils.ECVRF{
			PublicKeyBytes:  []byte("sji23sdvkn"),
			PrivateKeyBytes: []byte("27r2jfkajjkf"),
		}
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		Convey("success", func() {

			clientP := &sdk.ChainClient{}
			patch1 := ApplyMethod(reflect.TypeOf(processor),
				"GetChainClient", func(thip *ContractProcessor,
					chainId string) (*sdk.ChainClient, error) {
					return &sdk.ChainClient{}, nil
				})
			defer patch1.Reset()

			patch2 := ApplyMethod(reflect.TypeOf(clientP),
				"CreateContractUpgradePayload",
				func(cp *sdk.ChainClient,
					contractName, version,
					byteCodeStringOrFilePath string,
					runtime common.RuntimeType,
					kvs []*common.KeyValuePair) (*common.Payload, error) {
					return &common.Payload{
						ChainId: "chain1_alias",
						TxId:    "fwjiej327239",
					}, nil
				})
			defer patch2.Reset()
			patch3 := ApplyFunc(GetEndorsersWithAuthType, func(hashType crypto.HashType,
				authType sdk.AuthType, payload *common.Payload,
				signs []config.Sign) ([]*common.EndorsementEntry, error) {
				return []*common.EndorsementEntry{{
					Signature: []byte{'d'},
				}}, nil
			})
			defer patch3.Reset()
			patch4 := ApplyMethod(reflect.TypeOf(clientP),
				"SendContractManageRequest", func(cp *sdk.ChainClient,
					payload *common.Payload,
					endorsers []*common.EndorsementEntry,
					timeout int64, withSyncResult bool) (*common.TxResponse, error) {
					return &common.TxResponse{
						Code:    0,
						Message: "",
						TxId:    "2323dfaaf",
						ContractResult: &common.ContractResult{
							Code:    0,
							Result:  []byte("success"),
							Message: "",
							GasUsed: 12,
						},
					}, nil
				})
			defer patch4.Reset()

			patch5 := ApplyFunc(CheckProposalRequestResp,
				func(resp *common.TxResponse, needContractResult bool) error {
					return nil
				})
			defer patch5.Reset()
			err := processor.UpdateContract("chain1_alias", "oracle_contract_v1", "1",
				"../contract_files/chain1/oracle_contract_v1/1/oracle_contract_v1.7z",
				1)
			So(err, ShouldBeNil)
		})
	})

}

func TestDeployContract(t *testing.T) {
	Convey("DeployContract", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		vrf_utils.GlobalECVRF = &vrf_utils.ECVRF{
			PublicKeyBytes:  []byte("sji23sdvkn"),
			PrivateKeyBytes: []byte("27r2jfkajjkf"),
		}
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		Convey("success", func() {

			clientP := &sdk.ChainClient{}
			patch1 := ApplyMethod(reflect.TypeOf(processor),
				"GetChainClient", func(thip *ContractProcessor,
					chainId string) (*sdk.ChainClient, error) {
					return &sdk.ChainClient{}, nil
				})
			defer patch1.Reset()

			patch2 := ApplyMethod(reflect.TypeOf(clientP),
				"CreateContractCreatePayload",
				func(cp *sdk.ChainClient,
					contractName, version,
					byteCodeStringOrFilePath string,
					runtime common.RuntimeType,
					kvs []*common.KeyValuePair,
				) (*common.Payload, error) {
					return &common.Payload{
						ChainId: "chain1_alias",
						TxId:    "fwjiej327239",
					}, nil
				})
			defer patch2.Reset()
			patch3 := ApplyFunc(GetEndorsersWithAuthType, func(hashType crypto.HashType,
				authType sdk.AuthType, payload *common.Payload,
				signs []config.Sign) ([]*common.EndorsementEntry, error) {
				return []*common.EndorsementEntry{{
					Signature: []byte{'d'},
				}}, nil
			})
			defer patch3.Reset()
			patch4 := ApplyMethod(reflect.TypeOf(clientP),
				"SendContractManageRequest", func(cp *sdk.ChainClient,
					payload *common.Payload,
					endorsers []*common.EndorsementEntry,
					timeout int64, withSyncResult bool) (*common.TxResponse, error) {
					return &common.TxResponse{
						Code:    0,
						Message: "",
						TxId:    "2323dfaaf",
						ContractResult: &common.ContractResult{
							Code:    0,
							Result:  []byte("success"),
							Message: "",
							GasUsed: 12,
						},
					}, nil
				})
			defer patch4.Reset()

			patch5 := ApplyFunc(CheckProposalRequestResp,
				func(resp *common.TxResponse, needContractResult bool) error {
					return nil
				})
			defer patch5.Reset()
			err := processor.DeployContract("chain1_alias", "oracle_contract_v1", "1",
				"../contract_files/chain1/oracle_contract_v1/1/oracle_contract_v1.7z",
				1)
			So(err, ShouldBeNil)
		})
	})
}

// func TestDeployContractAndSubScribe(t *testing.T) {
// 	Convey("DeployContractAndSubScribe", t, func() {
// 		config.ReadConfigFile("../config_files/smart_oracle.yml")
// 		os.Chdir("../")
// 		vrf_utils.GlobalECVRF = &vrf_utils.ECVRF{
// 			PublicKeyBytes:  []byte("sji23sdvkn"),
// 			PrivateKeyBytes: []byte("27r2jfkajjkf"),
// 		}
// 		//在这里把他mock住
// 		patch := ApplyFunc(sdk.NewChainClient,
// 			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
// 				return &sdk.ChainClient{}, nil
// 			})
// 		defer patch.Reset()
// 		processor, err := NewContractProcesser()
// 		if err != nil {
// 			return
// 		}
// 		Convey("success", func() {

// 			patch1 := ApplyMethod(reflect.TypeOf(processor),
// 				"DeployAndUpdateDB", func(thip *ContractProcesor,
// 					name, filePath, chainAlias, chainId string,
// 					version int, needOperate int) (bool, error) {
// 					return true, nil
// 				})
// 			defer patch1.Reset()

// 			patch2 := ApplyFunc(models.QueryContractInfo, func(chainId, contractName string) (*models.ContractInfo, error) {
// 				return &models.ContractInfo{
// 					TopicQueryStart:  1,
// 					TopicFinishStart: 1,
// 				}, nil
// 			})
// 			defer patch2.Reset()

// 			patch4 := ApplyMethod(reflect.TypeOf(processor),
// 				"SubscribeContracts", func(cp *ContractProcesor,
// 					chainId,
// 					contractName, topic string,
// 					start, end int64) error {
// 					return nil
// 				})
// 			defer patch4.Reset()

// 			processor.DeployContractAndSubScribe("chain1_alias", "oracle_contract_v1", "1",
// 				"../contract_files/chain1/oracle_contract_v1/1/oracle_contract_v1.7z",
// 				1)
// 		})
// 	})
// }

func TestListInstalledOracleContracts(t *testing.T) {
	Convey("DeployContractAndSubScribe", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		vrf_utils.GlobalECVRF = &vrf_utils.ECVRF{
			PublicKeyBytes:  []byte("sji23sdvkn"),
			PrivateKeyBytes: []byte("27r2jfkajjkf"),
		}
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient,
			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
				return &sdk.ChainClient{}, nil
			})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			return
		}
		Convey("success", func() {

			patch2 := ApplyFunc(models.LoadContracts, func() ([]models.ContractInfo, error) {
				return []models.ContractInfo{{
					TopicQueryStart:  1,
					TopicFinishStart: 1,
					ChainId:          "chain1",
					ChainAlias:       "chain1_alias",
				}, {
					TopicQueryStart:  1,
					TopicFinishStart: 1,
					ChainId:          "chain2_alias",
				}}, nil
			})
			defer patch2.Reset()

			ret, _ := processor.ListInstalledOracleContracts("chain1_alias")
			So(len(ret), ShouldEqual, 1)
		})
	})
}
