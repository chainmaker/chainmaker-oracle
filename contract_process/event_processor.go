/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/models"
	"chainmaker-oracle/models/cross_chain"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"time"

	"chainmaker.org/chainmaker/pb-go/v2/common"
	"github.com/robfig/cron/v3"
)

// dealEvent 处理链上下发的事件
// @param *common.ContractEventInfo
func (cp *ContractProcessor) dealEvent(event *common.ContractEventInfo) {
	cp.Log.Infof("events is :%+v", event)
	switch event.Topic {
	case topicOracleQuery:
		cp.ProcessEventQuery(event) // 处理查询事件
	case topicOracleFinished:
		cp.ProcessEventFinished(event) // 处理完成事件
		return
	default:
		cp.Log.Errorf("event type wrong : %+v", event)
		return
	}

}

// ProcessEventQuery 处理监听到链上下发的query状态的事件
// @param *common.ContractEventInfo
func (cp *ContractProcessor) ProcessEventQuery(event *common.ContractEventInfo) {
	if len(event.EventData) != 6 {
		cp.Log.Errorf("processEventQuery eventdata illegal, %+v", event)
		return
	}
	eventState := makeDBEventStateFromEvent(event) // 准备数据库模型
	// 抢一下锁
	gotLockErr := models.InsertEventLock(&models.EventLock{
		RequestId:     eventState.RequestId,
		LockedVersion: 1,
	})
	needWatchJob := false
	if gotLockErr != nil {
		cp.Log.Warnf("processEventQuery insert lockerr , %+v ", gotLockErr.Error())
		needWatchJob = true
	} else {
		// 保存事件状态到数据库
		insertErr := models.InsertEventState(eventState)
		if insertErr != nil {
			cp.Log.Warnf("processEventQuery insert.err, %+v", insertErr.Error())
			needWatchJob = true
		}
	}
	if needWatchJob {
		// 说明别的服务已经处理了， 在这里需要增加一个监控任务
		cp.addWatchUnFinishedJob(eventState.ChainAlias, eventState.RequestId, eventState.TxId)
		cp.Log.Infof("processEventQuery miss lock, event(%+v), add watch job  ", eventState)
		return
	}
	// 更新一下合约监听事件的起始位置
	updateErr := models.UpdateContractTopicQueryStart(eventState.ChainAlias, event.ContractName, event.BlockHeight)
	if updateErr != nil {
		cp.Log.Warnf("processEventQuery UpdateContractTopicQueryStart err , %s ", updateErr.Error())
	}
	// 放入状态机跑一下
	runErr := cp.Run(eventState.ChainAlias, eventState.TxId, eventState.RequestId)
	if runErr != nil {
		cp.Log.Warnf("processEventQuery run err , %s ", runErr.Error())
	}
}

// ProcessEventFinished 处理监听到链上下发的结束状态的事件
// @param *common.ContractEventInfo
func (cp *ContractProcessor) ProcessEventFinished(event *common.ContractEventInfo) {
	// []string{chainAlias,tx id,request id,}
	// 校验一下参数
	if len(event.EventData) != 4 {
		cp.Log.Errorf("processEventFinished eventdata illegal, %+v ", event)
		return
	}
	chainAlias := event.EventData[0]
	txId := event.EventData[2]
	requestId := event.EventData[1]
	// 数据库中先查询一下状态
	eventData, queryErr := models.QueryProcessEvent(requestId)
	if queryErr != nil {
		cp.Log.Errorf("processEventFinished QueryProcessEvent  event (%+v), error  %s ", event, queryErr.Error())
		return
	}
	// 如果有，删除定时任务
	defer cp.RemoveCronJob(chainAlias, txId, requestId)
	if eventData.State == models.StateResultOnChainSuccess {
		return // 已经上链了
	}
	eventData.State = models.StateResultOnChainSuccess
	// 将已上链的状态更新一下
	updateErr := models.UpdateProcessEvent(eventData)
	if updateErr != nil {
		cp.Log.Warnf("processEventFinished UpdateProcessEvent (%+v) ,error %s ", eventData, updateErr.Error())
		return
	}
	// 更新一下监听finish事件的起始位置
	updateFinishErr := models.UpdateContractTopicFinishStart(chainAlias, event.ContractName, event.BlockHeight)
	if updateFinishErr != nil {
		cp.Log.Errorf("processEventFinished UpdateContractTopicFinishStart err , %s", updateFinishErr.Error())
		return
	}
	cp.Log.Infof("processEventFinished success, %+v", event)
}

// makeDBEventStateFromEvent 从链上监听事件转换为事件的数据库模型
// @param *common.ContractEventInfo
// @return *models.EventState
func makeDBEventStateFromEvent(event *common.ContractEventInfo) *models.EventState {
	// []string{queryMysql, chainAlias, requestId, txId, strconv.Itoa(blockHeight), string(mysqlQuery)}
	// []string{queryHttp, chainAlias, requestId, txId, strconv.Itoa(blockHeight), string(httpQuery)}
	queryType, _ := strconv.Atoi(event.EventData[0])
	chainAlias := event.EventData[1]
	requestId := event.EventData[2]
	txId := event.EventData[3]
	blockHeight, _ := strconv.ParseInt(event.EventData[4], 10, 64)
	queryBody := []byte(event.EventData[5])
	// 构造事件状态数据库模型
	return &models.EventState{
		ChainAlias:    chainAlias,
		RequestId:     requestId,
		TxId:          txId,
		State:         models.StateInit,
		QueryType:     models.EventQueryType(queryType),
		NextFibonacci: 0,
		BlockHeight:   blockHeight,
		QueryBody:     queryBody,
		ContractName:  event.ContractName,
	}
}

// RemoveCronJob 移除定时任务
// @param string
// @param string
// @param string
func (cp *ContractProcessor) RemoveCronJob(chainAlias string, txId string, requestId string) {
	// 计算一下hash
	mKey := models.ComputeHashKey(chainAlias, txId, requestId)
	// 看看当前有没有正在处理，如果没有返回
	mData, mOk := cp.EventInProcess.Load(mKey)
	if !mOk {
		return
	}
	mEvent, _ := mData.(EventV)
	if mEvent.State == globalJobIsCron {
		cp.CronManager.Remove(cron.EntryID(mEvent.EntryId)) // 删除监视的任务
	}
	cp.EventInProcess.Delete(mKey) // 删除内存中的记录
	cp.Log.Infof("RemoveCronJob chainAlias(%s),txId(%s),"+
		"requestId(%s),entryId(%d)", chainAlias,
		txId, requestId, mEvent.EntryId)
}

// Fetch 根据传入参数，跨链查询实现Fetch接口，
// @param string
// @return []byte
// @return error
func (cp *ContractProcessor) Fetch(fetchCfg string) ([]byte, error) {
	var tempQueryChain cross_chain.CrossChainQuery
	// 从json中复原出查询模型
	uErr := json.Unmarshal([]byte(fetchCfg), &tempQueryChain)
	if uErr != nil {
		cp.Log.Errorf("Fetch unmarshal error(%s) ", uErr.Error())
		return nil, uErr
	}
	var params []*common.KeyValuePair
	for i := 0; i < len(tempQueryChain.Params); i++ {
		var tKV common.KeyValuePair
		tKV.Key = tempQueryChain.Params[i].Key
		tKV.Value = tempQueryChain.Params[i].Value
		params = append(params, &tKV)
	}
	params = append(params, &common.KeyValuePair{
		Key:   "method",
		Value: []byte(tempQueryChain.MethodName),
	})
	// 查询链上的合约方法，查询数据
	resp, respErr := cp.QueryContract(tempQueryChain.ChainAlias,
		tempQueryChain.ContractName, invokeMethod, params)
	if respErr != nil {
		return []byte{}, respErr
	}
	if resp == nil || resp.ContractResult == nil {
		return []byte{}, errors.New("QueryContract no result")
	}
	if resp.Code > 0 {
		return []byte{}, fmt.Errorf("code(%d),message(%s)", resp.Code, resp.Message)
	}
	return resp.ContractResult.Result, nil
}

var (
	watchTypeFibonacci     = int(1)           // watchTypeFibonacci fibonacci类型
	watchTypeFibonacciStr  = "FibonacciWatch" // watchTypeFibonacciStr fibonacci字符
	watchTypeCron          = int(2)           // watchTypeCron cron类型
	watchTypeCronStr       = "EventWatch"     // watchTypeCronStr cron字符
	monitorTypeContract    = int(3)           // monitorTypeContract 监控合约类型
	monitorTypeContractStr = "ContractWatch"  // monitorTypeContractStr 合约类型字符
)

// WatchCronJobs 监控任务类
type WatchCronJobs struct {
	JobType    int // required, 1 为菲波拿契定时任务FibonacciWatch；2为常规监视任务（指定的配置时间启动的类型）EventWatch;3为周期性的合约扫描任务ContractWatch
	ChainAlias string
	RequestId  string
	TxId       string
	Processor  *ContractProcessor // required
	EntryId    cron.EntryID       // required
}

// ToString 实现string方法，便于打印
// @param time.Time
// @return string
func (jobs *WatchCronJobs) ToString(serverTime time.Time) string {
	scheduleNow := serverTime.String()
	jobTypeS := watchTypeFibonacciStr
	// 检查一下是否为事件监控
	if jobs.JobType == watchTypeCron {
		jobTypeS = watchTypeCronStr
	} else if jobs.JobType == monitorTypeContract {
		// 检查一下是否为合约监控类型
		jobTypeS = monitorTypeContractStr
	}
	return fmt.Sprintf("WatchCronJobs scheduled at(%s), JobType(%s),"+
		" chainAlias(%s), RequestId(%s),TxId(%s),EntryId(%d)  ",
		scheduleNow, jobTypeS, jobs.ChainAlias,
		jobs.RequestId, jobs.TxId, int(jobs.EntryId))
}

// Run 定时任务run，实现了cron的定时任务接口
func (jobs *WatchCronJobs) Run() {
	serverTime := time.Now()
	jobs.Processor.Log.Info(jobs.ToString(serverTime))
	if jobs.JobType == monitorTypeContract {
		// 如果是定时扫描数据库任务，扫一下数据库，订阅一下未订阅的合约事件
		_ = jobs.Processor.SubscribeContractEvents()
		return
	}
	// 根据请求id查询一下事件状态
	eventState, qErr := models.QueryProcessEvent(jobs.RequestId)
	if qErr != nil {
		jobs.Processor.Log.Errorf("job(%+v) query event err(%s) ", jobs, qErr.Error())
		return
	}
	// 如果事件状态为已上链，回调已成功，fibonacci重试完成，则删除改任务
	if eventState.State == models.StateCallbackSuccess ||
		eventState.State == models.StateCallbackFailedFibonacci ||
		eventState.State == models.StateResultOnChainSuccess {
		jobs.Processor.Log.Infof("job(%+v) watch event(%+v) done", jobs, eventState)
		// 内存中删除定时任务
		jobs.Processor.RemoveCronJob(jobs.ChainAlias, jobs.TxId, jobs.RequestId)
		// 定时器中删除定时任务
		jobs.Processor.CronManager.Remove(jobs.EntryId)
		return
	}
	// 根据类型区分一下是菲波那其还是观测任务
	if jobs.JobType == watchTypeFibonacci {
		// 定时器中删除定时任务
		jobs.Processor.CronManager.Remove(jobs.EntryId)
		// 内存中删除定时任务
		jobs.Processor.RemoveCronJob(jobs.ChainAlias, jobs.TxId, jobs.RequestId)
		// 放入状态机中运行
		_ = jobs.Processor.Run(jobs.ChainAlias, jobs.TxId, jobs.RequestId)
		return
	}
	iNow := time.Now()
	i30Before := iNow.Add(-1 * time.Second * time.Duration(config.GlobalCFG.WorkCFG.ProcessOvertime))
	// 拿一下锁
	gotLockErr := models.UpdateEventLock(&models.EventLock{
		RequestId: jobs.RequestId,
		UpdatedAt: i30Before,
	})
	if gotLockErr == nil {
		// 拿到锁，将事件放入状态机跑一下
		_ = jobs.Processor.Run(jobs.ChainAlias, jobs.TxId, jobs.RequestId)
	} else {
		return
	}
}
