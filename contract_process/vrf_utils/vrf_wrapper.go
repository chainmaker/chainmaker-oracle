/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package vrf_utils

import (
	"chainmaker-oracle/models"
	"crypto/ecdsa"
	"encoding/json"
	"math/big"
)

// VRFGetter 计算vrf工作类
type VRFGetter struct {
}

// Fetch 根据传入参数，计算VRF数据，将其json序列化
// @param string
// @return []byte
// @return error
func (g *VRFGetter) Fetch(fetchConfig string) ([]byte, error) {
	pi, hash, err := Prove([]byte(fetchConfig))
	if err != nil {
		return []byte(""), err
	}
	var retData models.Random
	retData.Pi = pi
	var bn big.Int
	bns := bn.SetBytes(hash)
	retData.RandData = bns.String()
	ratio, ratioErr := ComputeRatioFromHash(hash)
	if ratioErr != nil {
		return []byte(""), ratioErr
	}
	retData.Ratio = ratio
	ubs, _ := json.Marshal(retData)
	return ubs, nil
}

// VRFVerifier vrf 验证工作类
type VRFVerifier struct {
}

// Fetch 根据传入的参数，验证vrf是否正确，将结果json序列化
// @param string
// @return []byte
// @return error
func (v *VRFVerifier) Fetch(fetchConfig string) ([]byte, error) {
	var cfg models.Verify
	err := json.Unmarshal([]byte(fetchConfig), &cfg)
	if err != nil {
		return []byte(""), err
	}
	valid, vErr := Verify(cfg.Pi, []byte(cfg.Alpha))
	if vErr != nil {
		return []byte(""), vErr
	}
	if valid {
		return []byte("true"), nil
	}
	return []byte("false"), nil
}

// Marshal 将私钥json序列化
// @return []byte
func (k *PrivateKey) Marshal() []byte {
	var curvePK CurvePk
	curvePK.Key = k.D
	curvePK.X = k.X
	curvePK.Y = k.Y
	jbs, _ := json.Marshal(curvePK)
	return jbs
}

// Marshal 将公钥json序列化
// @return []byte
func (pk *PublicKey) Marshal() []byte {
	var marshalP CurvePoint
	marshalP.X = pk.X
	marshalP.Y = pk.Y
	bs, _ := json.Marshal(marshalP)
	return bs
}

// NewPublicKeyFromRawBytes 从数据库汇总存储的公钥计算出公钥实体类
// @param []byte
// @return *PublicKey
// @return error
func NewPublicKeyFromRawBytes(pubKeyBs []byte) (*PublicKey, error) {
	//从json字符串反序列化，构造出public key
	var marshalP CurvePoint
	err := json.Unmarshal(pubKeyBs, &marshalP)
	if err != nil {
		return nil, err
	}
	var retK PublicKey
	retK.PublicKey = &ecdsa.PublicKey{
		Curve: curve,
		X:     marshalP.X,
		Y:     marshalP.Y,
	}
	return &retK, nil
}

// NewPrivateKeyFromRawBytes 从数据库汇总存储的私钥计算出私钥实体类
// @param []byte
// @return *PrivateKey
// @return error
func NewPrivateKeyFromRawBytes(privateBs []byte) (*PrivateKey, error) {
	//从json字符串反序列化，构造出public key
	var marshalP CurvePk
	err := json.Unmarshal(privateBs, &marshalP)
	if err != nil {
		return nil, err
	}
	var retPk PrivateKey
	retPk.PrivateKey = &ecdsa.PrivateKey{
		D: marshalP.Key,
		PublicKey: ecdsa.PublicKey{
			Curve: curve,
			X:     marshalP.X,
			Y:     marshalP.Y,
		},
	}
	return &retPk, nil
}

// ComputeRatioFromHash 根据hash计算随机数[0,1]
// @param []byte
// @return float64
// @return error
func ComputeRatioFromHash(hash []byte) (float64, error) {
	//sha256 共计算32个byte
	t := &big.Int{}
	t.SetBytes(hash)
	precision := uint(8 * (len(hash) + 1))
	max, b, err := big.ParseFloat("0xfffffffffffffffffffffffffff"+
		"fffffffffffffffffffffffffffffffffffff",
		0, precision, big.ToNearestEven)
	if b != 16 || err != nil {
		return float64(-1), err
	}
	h := big.Float{}
	h.SetPrec(precision)
	h.SetInt(t)
	ratio := big.Float{}
	ret, _ := ratio.Quo(&h, max).Float64()
	return ret, nil
}
