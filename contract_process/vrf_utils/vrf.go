/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

// Package vrf_utils 提供VRF功能
package vrf_utils

import (
	"chainmaker-oracle/models"
)

// ECVRF vrf业务处理类
type ECVRF struct {
	PublicKeyBytes  []byte
	PrivateKeyBytes []byte
	PrivateKey      *PrivateKey
	PublicKey       *PublicKey
}

var (
	GlobalECVRF *ECVRF // GlobalECVRF vrf处理实体类（单体）
)

// InitVRFProcessor 初始化VRF模块
// @param *models.VRFStore
// @return error
func InitVRFProcessor(vrfDb *models.VRFStore) error {
	GlobalECVRF = &ECVRF{
		PrivateKeyBytes: vrfDb.PrivateKeys,
		PublicKeyBytes:  vrfDb.PublicKeys,
	}
	privateKey, err := NewPrivateKeyFromRawBytes(GlobalECVRF.PrivateKeyBytes)
	if err != nil {
		return err
	}
	GlobalECVRF.PrivateKey = privateKey
	publicKey, err := NewPublicKeyFromRawBytes(GlobalECVRF.PublicKeyBytes)
	if err != nil {
		return err
	}
	GlobalECVRF.PublicKey = publicKey
	return nil
}

// Prove 根据原始消息生成可信随机数和证据
// @param []byte
// @return []byte
// @return []byte
// @return error
func Prove(msg []byte) ([]byte, []byte, error) {
	hash32, proof := GlobalECVRF.PrivateKey.Evaluate(msg)
	return proof, hash32[:], nil
}

// Verify 根据所传证据和原始消息验证随机数是否可信
// @param []byte
// @param []byte
// @return bool
// @return error
func Verify(pi, msg []byte) (bool, error) {
	_, err := GlobalECVRF.PublicKey.ProofToHash(msg, pi)
	if err != nil {
		return false, err
	}
	return true, nil
}

// GenerateKeys 生成vrf公私钥
// @return *models.VRFStore
// @return error
func GenerateKeys() (*models.VRFStore, error) {
	var vrfDb models.VRFStore
	privateKey, publicKey := GenerateKey()
	vrfDb.PrivateKeys = privateKey.Marshal()
	vrfDb.PublicKeys = publicKey.Marshal()
	return &vrfDb, nil
}
