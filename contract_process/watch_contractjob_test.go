/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

import (
	"chainmaker-oracle/config"
	"os"
	"testing"

	sdk "chainmaker.org/chainmaker/sdk-go/v2"

	. "github.com/agiledragon/gomonkey"

	. "github.com/smartystreets/goconvey/convey"
)

func TestMonitorContract(t *testing.T) {
	Convey("MonitorContract", t, func() {
		config.ReadConfigFile("../config_files/smart_oracle.yml")
		os.Chdir("../")
		//在这里把他mock住
		patch := ApplyFunc(sdk.NewChainClient, func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
			return &sdk.ChainClient{}, nil
		})
		defer patch.Reset()
		processor, err := NewContractProcessor()
		if err != nil {
			t.Fail()
			return
		}
		processor.MonitorContract()
	})

}
