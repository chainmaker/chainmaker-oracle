/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package contract_process

// import (
// 	"chainmaker-oracle/config"
// 	"chainmaker-oracle/contract_process/vrf_utils"
// 	"chainmaker-oracle/models"
// 	"os"
// 	"reflect"
// 	"testing"
// 	"time"

// 	sdk "chainmaker.org/chainmaker/sdk-go/v2"
// 	. "github.com/agiledragon/gomonkey"
// 	. "github.com/smartystreets/goconvey/convey"
// )

// func TestRetrieveInProcessingJob(t *testing.T) {
// 	Convey("RetrieveInProcessingJob", t, func() {
// 		config.ReadConfigFile("../config_files/smart_oracle.yml")
// 		os.Chdir("../")
// 		vrf_utils.GlobalConcretECVRF = &vrf_utils.ConcretECVRF{
// 			PublicKeyBytes:  []byte("sji23sdvkn"),
// 			PrivateKeyBytes: []byte("27r2jfkajjkf"),
// 		}
// 		//在这里把他mock住
// 		patch := ApplyFunc(sdk.NewChainClient,
// 			func(ops ...sdk.ChainClientOption) (*sdk.ChainClient, error) {
// 				return &sdk.ChainClient{}, nil
// 			})
// 		defer patch.Reset()
// 		processor, err := NewContractProcesser()
// 		if err != nil {
// 			return
// 		}
// 		Convey("success", func() {

// 			patch2 := ApplyFunc(models.QueryInProcessEvents, func(queryTime time.Time, page int) ([]models.EventState, error) {
// 				return []models.EventState{{
// 					RequestId:    "afdjakjf232",
// 					TxId:         "293ijkfljodda",
// 					ContractName: "oracle_contract_v1",
// 					State:        models.StateCallbackFailed,
// 					ChainAlias:   "chain1",
// 				}, {
// 					RequestId:    "afdjaffkjf232",
// 					TxId:         "293ijvxbkfljodda",
// 					ContractName: "oracle_contract_v1",
// 					State:        models.StateInit,
// 					ChainAlias:   "chain1",
// 				}}, nil
// 			})
// 			defer patch2.Reset()

// 			patch3 := ApplyMethod(reflect.TypeOf(processor),
// 				"Run", func(cp *ContractProcesor, chainId,
// 					txId, requestId string) error {
// 					return nil
// 				})
// 			defer patch3.Reset()

// 			patch4 := ApplyFunc(models.UpdateEventLock, func(*models.EventLock) error {
// 				return nil
// 			})
// 			defer patch4.Reset()
// 			processor.RetrieveInProcessingJob(time.Now())
// 		})
// 	})
// }
