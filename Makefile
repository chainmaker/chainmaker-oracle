build:
	go build .

ut:
	./ut_cover.sh

lint:
	golangci-lint run ./...