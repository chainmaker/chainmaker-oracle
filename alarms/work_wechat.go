/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package alarms

import (
	"bytes"
	"chainmaker-oracle/config"
	"chainmaker-oracle/models"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
)

// 企业微信发送告警消息到指定群id

// WeixinAlarmSender 企业微信报警实现的类
type WeixinAlarmSender struct {
}

// SendAlarm 实现推送信息到企业微信机器人
// @param string
// @return error
func (alamer *WeixinAlarmSender) SendAlarm(notice string) error {

	weixinMessage := models.WeixinTextMessage{
		MsgType: models.GlobalWorkWeixinTypeText,

		Text: models.TextContent{
			Content: notice,
		},
	}
	messageBs, _ := json.Marshal(weixinMessage)
	bsReader := bytes.NewReader(messageBs)
	response, responseErr := http.DefaultClient.Post(config.GlobalCFG.AlarmSenderWeixin.WebHook,
		"application/json", bsReader)
	if responseErr != nil {
		return responseErr
	}
	if response == nil || response.Body == nil {
		return errors.New("no response")
	}

	defer response.Body.Close()
	resBs, resBsErr := io.ReadAll(response.Body)
	if resBsErr != nil {
		return fmt.Errorf("read response err, %s ", resBsErr.Error())
	}
	var retResp Ret
	err := json.Unmarshal(resBs, &retResp)
	if err != nil {
		return fmt.Errorf("parse response body err, %s ", err.Error())
	}
	if retResp.ErrCode != 0 {
		return fmt.Errorf("respMsg : %+v ", retResp)
	}
	return nil
}

// Ret 用于接收http请求的json数据
type Ret struct {
	ErrCode int    `json:"errcode"`
	ErrMsg  string `json:"errmsg"`
}
