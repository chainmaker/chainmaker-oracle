/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

// Package alarms provides alarm tools, using work-wechat or dingding robot
package alarms
