/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package alarms

import "chainmaker-oracle/config"

// AlarmSender 接口定义了发送报警信息的方法
type AlarmSender interface {
	SendAlarm(notice string) error
}

// GetAlarmSender 根据配置文件中配置的报警类型，返回实现报警接口的实例，当前仅支持2种：weixin,dingding,
func GetAlarmSender() AlarmSender {
	alarmType := config.GlobalCFG.AlarmSenderType.Type
	switch alarmType {
	case "weixin":
		return &WeixinAlarmSender{}
	case "dingding":
		return &DingDingAlarmSender{}
	default:
		return &DummyAlarmSender{}
	}
}

// DummyAlarmSender 实现SendAlarm接口的一个实例，当用户不打算将报警信息推送到机器人的时候，可以使用这个类型（默认类型）
type DummyAlarmSender struct {
}

// SendAlarm SendAlarm接口的空实现，啥也不做
// @param string
// @return error
func (d *DummyAlarmSender) SendAlarm(string) error {
	return nil
}
