/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package alarms

import (
	"fmt"
	"testing"
)

func TestSendAlarm2(t *testing.T) {
	initTestTools()
	ding := &DingDingAlarmSender{}
	err := ding.SendAlarm("on chain error xx")
	if err != nil {
		fmt.Printf("sendalram err , %s  ", err.Error())
	}
}
