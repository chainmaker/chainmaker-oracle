/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package alarms

import (
	"bytes"
	"chainmaker-oracle/config"
	"chainmaker-oracle/models"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"time"
)

// DingDingAlarmSender 定义了发送钉钉机器人消息的工作类
type DingDingAlarmSender struct {
}

// sign 钉钉签名
// @return string
func (d *DingDingAlarmSender) sign() string {
	timeMills := time.Now().UnixNano() / 1e6
	stringToSign := fmt.Sprintf("%d %s", timeMills, config.GlobalCFG.AlarmSenderDingDing.Secret)
	mac := hmac.New(sha256.New, []byte(config.GlobalCFG.AlarmSenderDingDing.Secret))
	mac.Write([]byte(stringToSign))
	sign := base64.StdEncoding.EncodeToString(mac.Sum(nil))
	retStr := fmt.Sprintf("%s&timestamp=%d&sign=%s", config.GlobalCFG.AlarmSenderDingDing.WebHook, timeMills, sign)
	return retStr
}

// SendAlarm 实现了SendAlarm接口，可以推送消息到钉钉机器人
// @param string
// @return error
func (d *DingDingAlarmSender) SendAlarm(notice string) error {
	callUrl := d.sign()
	dingdingMsg := models.DingdingMessage{
		MsgType: "text",
		Text: models.TextContent{
			Content: notice,
		},
	}
	messageBs, _ := json.Marshal(dingdingMsg)
	bsReader := bytes.NewReader(messageBs)
	response, responseErr := http.DefaultClient.Post(callUrl, "application/json", bsReader)
	if responseErr != nil {
		return responseErr
	}
	if response == nil || response.Body == nil {
		return errors.New("no response")
	}

	defer response.Body.Close()
	resBs, resBsErr := io.ReadAll(response.Body)
	if resBsErr != nil {
		return fmt.Errorf("read response err, %s ", resBsErr.Error())
	}
	var retResp models.DingdingResp
	err := json.Unmarshal(resBs, &retResp)
	if err != nil {
		return fmt.Errorf("parse response body err, %s ", err.Error())
	}
	if retResp.ErrCode != 0 {
		return fmt.Errorf("respMsg : %+v ", retResp)
	}
	return nil
}
