/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package config

// ProcessorConfig 服务配置
type ProcessorConfig struct {
	LogCFG              LogConfig               `mapstructure:"log"`
	SDKS                []SDKConfig             `mapstructure:"sdks"`
	WorkCFG             WorkLoadConfig          `mapstructure:"worker_loads"`
	OracleMysqlCFG      MySqlConfig             `mapstructure:"oracle_mysql"`
	DataSourceMysqlCFG  []DataSourceMySqlConfig `mapstructure:"datasource_mysql"`
	HttpServerCFG       HttpServerConfig        `mapstructure:"http_server"`
	AlarmSenderType     AlarmSender             `mapstructure:"alarm_sender"`
	AlarmSenderWeixin   AlarmSenderWeixin       `mapstructure:"alarm_sender_weixin"`
	AlarmSenderDingDing AlarmSenderDingding     `mapstructure:"alarm_sender_dingding"`
}

// WorkLoadConfig 预言机服务配置
type WorkLoadConfig struct {
	ProcessorCnts       int    `mapstructure:"processor_cnts"`
	MessageBuffers      int    `mapstructure:"message_buffers"`
	FibonacciLimits     int    `mapstructure:"fibonacci_limits"`
	FibonacciTimeunit   int    `mapstructure:"fibonacci_timeunit"`
	ProcessOvertime     int    `mapstructure:"process_overtime"`
	MonitorContractTime int    `mapstructure:"monitor_contract_time"`
	OracleContractName  string `mapstructure:"oracle_contract_name"`
}

// MySqlConfig mysql数据库配置
type MySqlConfig struct {
	UserName       string `mapstructure:"user_name"`
	Password       string `mapstructure:"password"`
	DbAddress      string `mapstructure:"db_address"`
	Database       string `mapstructure:"database"`
	MaxOpenConns   int    `mapstructure:"max_open"`
	MaxIdleConns   int    `mapstructure:"max_idle"`
	MaxIdleSeconds int    `mapstructure:"max_idle_seconds"`
	MaxLifeSeconds int    `mapstructure:"max_life_seconds"`
}

// DataSourceMySqlConfig 用户指定mysql数据源配置
type DataSourceMySqlConfig struct {
	DataSource     string `mapstructure:"datasource"`
	UserName       string `mapstructure:"user_name"`
	Password       string `mapstructure:"password"`
	DbAddress      string `mapstructure:"db_address"`
	Database       string `mapstructure:"database"`
	MaxOpenConns   int    `mapstructure:"max_open"`
	MaxIdleConns   int    `mapstructure:"max_idle"`
	MaxIdleSeconds int    `mapstructure:"max_idle_seconds"`
	MaxLifeSeconds int    `mapstructure:"max_life_seconds"`
}

// HttpServerConfig 预言机http服务配置
type HttpServerConfig struct {
	Port        string `mapstructure:"port"`
	RunMode     string `mapstructure:"run_mode"` // release or debug
	AdminToken  string `mapstructure:"admin_token"`
	AssistToken string `mapstructure:"assist_token"`
}

// AlarmSenderWeixin 企业微信webhook机器人配置
type AlarmSenderWeixin struct {
	WebHook string `mapstructure:"web_hook"` // 机器人地址
}

// AlarmSenderDingding 钉钉webhook机器人配置
type AlarmSenderDingding struct {
	WebHook string `mapstructure:"web_hook"` // 机器人地址
	Secret  string `mapstructure:"secret"`   // 密钥
}

// AlarmSender 报警类型配置
type AlarmSender struct {
	Type string `mapstructure:"type"`
}
