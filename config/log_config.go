/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package config

// LogConfig 日志配置信息
type LogConfig struct {
	LogInConsole bool   `mapstructure:"log_in_console"`
	ShowColor    bool   `mapstructure:"show_color"`
	LogLevel     string `mapstructure:"log_level"`
	LogPath      string `mapstructure:"log_path"`
}
