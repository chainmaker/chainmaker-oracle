/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

// Package config 配置文件夹
package config

import (
	"errors"
	"fmt"
	"os"

	"github.com/spf13/viper"
)

var (
	GlobalCFG *ProcessorConfig // GlobalCFG 解析到的yaml文件实例
)

// ReadConfigFile 从指定文件中读取配置信息到配置实例
// @param string
// @return error
func ReadConfigFile(confPath string) error {
	var (
		err       error
		confViper *viper.Viper
	)
	if confViper, err = initViper(confPath); err != nil {
		return fmt.Errorf("load sdk config failed, %s", err)
	}
	GlobalCFG = &ProcessorConfig{}
	if err = confViper.Unmarshal(GlobalCFG); err != nil {
		return fmt.Errorf("unmarshal config file failed, %s", err)
	}
	fmt.Fprintf(os.Stdout, "config is : %+v ", GlobalCFG)
	err = GlobalCFG.Verify()
	if err != nil {
		return err
	}
	return nil
}

// initViper 加载配置文件，解析
// @param string
// @return *viper.Viper
// @return error
func initViper(confPath string) (*viper.Viper, error) {
	cmViper := viper.New()
	cmViper.SetConfigFile(confPath)
	err := cmViper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	return cmViper, nil
}

// Verify 配置信息验证
// @return error
func (pc *ProcessorConfig) Verify() error {
	return pc.WorkCFG.Verify()
}

// Verify 验证配置
// @return error
func (wc *WorkLoadConfig) Verify() error {
	if wc.MonitorContractTime < 1 || wc.MonitorContractTime > 59 {
		return errors.New("monitor_contract_time must be in range [1,59]")
	}
	if wc.ProcessOvertime < 1 || wc.ProcessOvertime > 3599 {
		return errors.New("process_overtime must be in range [1,3599]")
	}
	if len(wc.OracleContractName) < 1 {
		return errors.New("oracle_contract_name can not be null")
	}
	return nil
}
