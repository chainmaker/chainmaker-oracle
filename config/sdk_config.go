/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package config

// SDKConfig sdk配置
type SDKConfig struct {
	ChainAlias string `mapstructure:"chain_alias"`
	ChainId    string `mapstructure:"chain_id"`
	ConfigPath string `mapstructure:"config_path"`
	Signs      []Sign `mapstructure:"signs"`
}

// Sign 签名配置
type Sign struct {
	UserKeyFilePath     string `mapstructure:"user_key_file_path"`
	UserCrtFilePath     string `mapstructure:"user_crt_file_path"`
	UserSignKeyFilePath string `mapstructure:"user_sign_key_file_path"`
	UserSignCrtFilePath string `mapstructure:"user_sign_crt_file_path"`
	OrgId               string `mapstructure:"org_id"`
}
