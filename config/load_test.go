/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package config

import (
	"fmt"
	"os"
	"path/filepath"
	"testing"
)

func TestReadConfigFile(t *testing.T) {
	dir, _ := os.Getwd()
	ReadConfigFile(fmt.Sprintf("%s/config_files/smart_oracle.yml", filepath.Dir(dir)))
	fmt.Printf("%+v ", GlobalCFG)
	for i := 0; i < len(GlobalCFG.SDKS); i++ {
		fmt.Printf("chainid: %s , configpath:  %s ", GlobalCFG.SDKS[i].ChainId, GlobalCFG.SDKS[i].ConfigPath)
	}
}
