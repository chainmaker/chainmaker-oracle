/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package fetch_data

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/models/smart_sql"
	"encoding/json"
	"fmt"
	"testing"
)

func TestFetchDatabaseFetch(t *testing.T) {
	config.ReadConfigFile("../config_files/smart_oracle.yml")
	InitDataSourceDB(config.GlobalCFG.DataSourceMysqlCFG)
	fet := &FetchDatabase{}
	sqlM := smart_sql.ModelSql{
		SqlType: "select",
		//PageNum:     1,
		//PageSize:    20,
		DataSource:  "local",
		SqlSentence: "select * from event_result;",
	}
	bs, _ := json.Marshal(sqlM)
	strs, strsErr := fet.Fetch(string(bs))
	if strsErr != nil {
		fmt.Printf("err(%s)  ", strsErr.Error())
		return
	}

	fmt.Printf("strs is %s  ", strs)
	var bm []map[string]interface{}
	json.Unmarshal(strs, &bm)
	fmt.Printf("rows: %+v  ", bm)
}
