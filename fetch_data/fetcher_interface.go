/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

// Package fetch_data 接口定义了根据查询配置查询数据的方法
package fetch_data

type Fetcher interface {
	Fetch(fetchCfg string) ([]byte, error) //Fetch 根据查询类型查询不同的数据
}
