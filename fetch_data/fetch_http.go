/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package fetch_data

import (
	"chainmaker-oracle/models/smart_http"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/antchfx/jsonquery"

	"github.com/antchfx/htmlquery"
	"github.com/antchfx/xmlquery"
)

var (
	sleepTime = 1 * time.Second // sleepTime 1s
)

const (
	htmlType = "html" // htmlType html类型
	jsonType = "json" // jsonType json类型
	textType = "text" // textType text类型
	xmlType  = "xml"  // xmlType xml类型
)

// FetchHttp 获取http接口数据类型
type FetchHttp struct {
}

// Fetch 根据指定配置取http数据，结果json序列化后返回
// @param string
// @return []byte
// @return error
func (fh *FetchHttp) Fetch(fetchConfig string) (ret []byte, retErr error) {
	errString := ""
	mh := &smart_http.ModelHttp{}
	// 从json中恢复出用户的查询模型
	retErr = json.Unmarshal([]byte(fetchConfig), mh)
	if retErr != nil {
		return
	}
	if mh.MaxRetry <= 0 || mh.MaxRetry > 10 { //这个地方指定为10 todo
		mh.MaxRetry = 3
	}
	if mh.ConnectionTimeout < 30 || mh.ConnectionTimeout > 60 {
		mh.ConnectionTimeout = 30
	}
	if mh.MaxFetchTimeout < 10 || mh.MaxFetchTimeout > 60 {
		mh.MaxFetchTimeout = 10
	}
	defer func() {
		recoErr := recover()
		if recoErr != nil {
			retErr = fmt.Errorf("FetchHttp panic(%+v) ", recoErr)
		}
	}()
	for i := 1; i <= mh.MaxRetry; i++ {
		ret, retErr = fh.sendHttpRequest(mh)
		if retErr == nil {
			return
		}
		errString += "httpRequest retryTimes:" + fmt.Sprintf("%d Error:", i) + retErr.Error() + " "

		time.Sleep(sleepTime * time.Duration(2*i+1))
	}
	return nil, errors.New("SendRetry err:" + errString)

}

// sendHttpRequest 发送http请求，结果json序列化后返回
// @param *smart_http.ModelHttp
// @return []byte
// @return error
func (fh *FetchHttp) sendHttpRequest(mh *smart_http.ModelHttp) ([]byte, error) {
	client := &http.Client{
		Timeout: time.Duration(mh.ConnectionTimeout) * time.Second,
	}
	// 生成要访问的url
	url := mh.URL
	// 提交请求
	reqest, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	for k, v := range mh.HttpHeader {
		reqest.Header.Add(k, v)
	}
	// 处理返回结果
	resp, err := client.Do(reqest)
	if err != nil {
		return nil, err
	}
	if resp == nil {
		return nil, errors.New("no data")
	}
	if resp.Header != nil {
		responseT := resp.Header.Get("Content-Type")
		if strings.Contains(responseT, htmlType) {
			mh.FetchDataType = htmlType
		} else if strings.Contains(responseT, xmlType) {
			mh.FetchDataType = xmlType
		} else if strings.Contains(responseT, jsonType) {
			mh.FetchDataType = jsonType
		} else if strings.Contains(responseT, "plain") {
			mh.FetchDataType = textType
		} else {
			mh.FetchDataType = "other"
		}
	}
	defer resp.Body.Close()

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil && data == nil {
		return nil, err
	}
	dataStr := string(data)
	if strings.TrimSpace(mh.FetchDataFormula) == "" {
		return data, nil
	}
	return parseHttpBody(dataStr, mh)
}

// parseHttpBody 解析结果，json序列化后返回
// @param string
// @param *smart_http.ModelHttp
// @return []byte
// @return error
func parseHttpBody(dataStr string, mh *smart_http.ModelHttp) ([]byte, error) {
	result := make([]string, 0)
	switch mh.FetchDataType {
	case htmlType:
		doc, err := htmlquery.Parse(strings.NewReader(dataStr))
		if err != nil {
			return nil, err
		}
		xpathData := htmlquery.Find(doc, mh.FetchDataFormula)
		for _, row := range xpathData {
			rowStr := htmlquery.InnerText(row)
			result = append(result, rowStr)
		}
		resBytes, err := json.Marshal(result)
		if err != nil {
			return nil, err
		}
		return resBytes, nil
	case xmlType:
		doc, err := xmlquery.Parse(strings.NewReader(dataStr))
		if err != nil {
			return nil, err
		}
		xpathData := xmlquery.Find(doc, mh.FetchDataFormula)
		for _, row := range xpathData {
			rowStr := row.InnerText()
			result = append(result, rowStr)
		}
		resBytes, err := json.Marshal(result)
		if err != nil {
			return nil, err
		}
		return resBytes, nil
	case jsonType:
		doc, err := jsonquery.Parse(strings.NewReader(dataStr))
		if err != nil {
			return nil, err
		}
		xpathData := jsonquery.Find(doc, mh.FetchDataFormula)
		for _, row := range xpathData {
			rowStr := row.InnerText()
			result = append(result, rowStr)
		}
		resBytes, err := json.Marshal(result)
		if err != nil {
			return nil, err
		}
		return resBytes, nil
	case textType:
		// 生成正则对象，类似Python中的re.compile
		// 特别说明,正则中的()是分组的意思
		oReg := regexp.MustCompile(mh.FetchDataFormula)
		// 在text中查找，-1位置的参数可以指定个数，-1代表匹配所有，类似于py中可以指定次数的search
		result := oReg.FindAllStringSubmatch(dataStr, -1)
		resBytes, err := json.Marshal(result)
		if err != nil {
			return nil, err
		}
		return resBytes, nil
	default:
		return []byte(dataStr), nil
	}
}
