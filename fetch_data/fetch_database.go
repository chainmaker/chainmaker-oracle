/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package fetch_data

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/models/smart_sql"
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"time"

	//mysql驱动包
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

const (
	localSqlDNS = "%s:%s@tcp(%s)/%s?charset" +
		"=utf8mb4&loc=Local&parseTime=" +
		"true&interpolateParams=false" // localSqlDNS 拼接sql的dns
)

// FetchDatabase 查询数据库的实体类
type FetchDatabase struct {
}

// dbMap key为数据源,value为mysql连接
var dbMap = make(map[string]*sqlx.DB) // dbMap 多mysql源支持

// CloseDataSource 关闭mysql数据源
func CloseDataSource() {
	// 遍历所有sql连接,依次关闭
	for dataSource, dbHandler := range dbMap {
		if dbHandler == nil {
			continue
		}
		// 关闭sql连接
		dbHandler.Close()
		fmt.Fprintf(os.Stdout, "datasource(%s) CloseDataSource  ", dataSource)

	}
}

//InitDataSourceDB 预言机启动后，预言机初始化时，需要初始化数据库，一次操作就可以
// @param []config.DataSourceMySqlConfig
// @return error
func InitDataSourceDB(cfgs []config.DataSourceMySqlConfig) (err error) {
	// 遍历数据库配置,依次建立连接
	for i := 0; i < len(cfgs); i++ {
		cfg := cfgs[i]
		dsn := fmt.Sprintf(localSqlDNS,
			cfg.UserName, cfg.Password, cfg.DbAddress, cfg.Database)
		// 也可以使用MustConnect连接不成功就panic
		// sqlx.Connect包含Open方法和ping方法
		db, err := sqlx.Connect("mysql", dsn)
		if err != nil {
			fmt.Printf("connect DB failed, err:%v ", err)
			continue
		}
		// 设置一下最大空连接
		db.SetMaxIdleConns(cfg.MaxIdleConns)
		// 设置一下最大连接
		db.SetMaxOpenConns(cfg.MaxOpenConns)
		// 设置一下最大空连接时长
		db.SetConnMaxIdleTime(time.Duration(cfg.MaxIdleSeconds) * time.Second)
		// 设置一下最大连接时长
		db.SetConnMaxLifetime(time.Duration(cfg.MaxLifeSeconds) * time.Second)
		fmt.Fprintf(os.Stdout, "datasource(%s) InitDataSourceDB  ", cfg.DataSource)
		// 将新建好的链接放到map中备用
		dbMap[cfg.DataSource] = db
	}
	return
}

// Fetch 获取mysql数据，将查到的数据放到一个[]map[string]interface{},序列化后返回
// @param string
// @return []byte
// @return error
func (fh *FetchDatabase) Fetch(fetchCfg string) ([]byte, error) {
	ms := &smart_sql.ModelSql{}
	// 取出待查询的sql信息
	json.Unmarshal([]byte(fetchCfg), ms)
	// 根据数据源从map中取一下sql连接句柄
	db, dbOk := dbMap[ms.DataSource]
	if !dbOk || db == nil {
		return []byte("no sql connection"), fmt.Errorf("no sql connection to datasource(%s)", ms.DataSource)
	}
	sqlStr := ms.SqlSentence
	// 根据语句查一下数据库
	rows, err := db.Query(sqlStr)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	// 取结果数据集中的列类型
	colTyps, _ := rows.ColumnTypes()
	// 取结果数据集中的列数组
	columns, _ := rows.Columns()
	// 取一下所有列的总数目
	columnLength := len(columns)
	cache := make([]interface{}, columnLength)
	// 生成scan的变量列表
	for index := 0; index < columnLength; index++ {
		if isStringConvert(colTyps[index].DatabaseTypeName()) {
			cache[index] = reflect.New(reflect.TypeOf("")).Interface()
		} else {
			cache[index] = reflect.New(colTyps[index].ScanType()).Interface()
		}
	}

	var list []map[string]interface{}
	// 遍历结果集
	for rows.Next() {
		// 依次扫描每行数据
		_ = rows.Scan(cache...)
		item := make(map[string]interface{})
		for i, data := range cache {
			// 将对应字段值放到map中
			item[columns[i]] = reflect.ValueOf(data).Elem().Interface()
		}
		// 搜集扫描出的每行数据
		list = append(list, item)
	}
	if rows.Err() != nil {
		return nil, rows.Err()
	}
	// 序列化一下数据
	listBytes, err := json.Marshal(list)
	if err != nil {
		return nil, err
	}

	return listBytes, nil
}

// isStringConvert 判断mysql的可读字符串类型
// @param string
// @return bool
func isStringConvert(typeName string) bool {
	if typeName == "TEXT" ||
		typeName == "LONGTEXT" ||
		typeName == "MEDIUMTEXT" ||
		typeName == "CHAR" ||
		typeName == "TINYTEXT" ||
		typeName == "VARCHAR" {
		return true
	}
	return false
}
