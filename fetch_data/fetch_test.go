/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package fetch_data

import (
	"chainmaker-oracle/models/smart_http"
	"fmt"
	"testing"
)

func TestFetchHTML(t *testing.T) {

	vc := &smart_http.ModelHttp{
		//Method:     "GET",
		URL:        "https://www.baidu.com",
		HttpHeader: map[string]string{"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3776.0 Safari/537.36"},
		//3*time.Second
		ConnectionTimeout: 5,  //最大连接超时时长，默认30s，单位秒级别
		MaxRetry:          3,  //最大连接试错次数，默认3次
		MaxFetchTimeout:   10, //最大读取超时时长，默认10s，可以选择为10-60s

		FetchDataType:    "html",    //json,xml,html,text
		FetchDataFormula: "//title", //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据,https://goessner.net/articles/JsonPath/(json)
	}
	bs := smart_http.NewHttpRequest(vc)
	fmt.Println(string(bs))

	fetcher := new(FetchHttp)

	s1, err := fetcher.Fetch(string(bs))
	if err != nil {
		fmt.Printf("err: %s", err.Error())
	}
	fmt.Println(string(s1))

}

func TestFetchXML(t *testing.T) {

	vc := &smart_http.ModelHttp{
		//Method:     "GET",
		URL:        "https://www.w3school.com.cn/example/xmle/note.xml",
		HttpHeader: map[string]string{},
		//3*time.Second
		ConnectionTimeout: 5,  //最大连接超时时长，默认30s，单位秒级别
		MaxRetry:          3,  //最大连接试错次数，默认3次
		MaxFetchTimeout:   10, //最大读取超时时长，默认10s，可以选择为10-60s

		FetchDataType:    "xml",    //json,xml,html,text
		FetchDataFormula: "//note", //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据,https://goessner.net/articles/JsonPath/(json)
	}
	bs := smart_http.NewHttpRequest(vc)
	fmt.Println(string(bs))

	fetcher := new(FetchHttp)

	s1, err := fetcher.Fetch(string(bs))
	if err != nil {
		fmt.Printf("err : %s ", err.Error())
	}
	fmt.Println(string(s1))
}

func TestFetchJSON(t *testing.T) {

	vc := &smart_http.ModelHttp{
		//Method:     "GET",
		URL: "https://v0.yiketianqi.com/api?unescape=1&version=v61&appid=88684831&appsecret=OsSX6jwW",
		//HttpHeader:        map[string]string{},
		ConnectionTimeout: 5,  //最大连接超时时长，默认30s，单位秒级别
		MaxRetry:          3,  //最大连接试错次数，默认3次
		MaxFetchTimeout:   10, //最大读取超时时长，默认10s，可以选择为10-60s

		FetchDataType:    "json",            //json,xml,html,text
		FetchDataFormula: "/==/update_time", //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据,https://goessner.net/articles/JsonPath/(json)
	}
	bs := smart_http.NewHttpRequest(vc)
	fmt.Println(string(bs))

	fetcher := new(FetchHttp)

	s1, err := fetcher.Fetch(string(bs))
	if err != nil {
		fmt.Printf("err: %s", err.Error())
	}
	fmt.Println(string(s1))
}

func TestFetchTEXT(t *testing.T) {

	vc := &smart_http.ModelHttp{
		//Method:     "GET",
		URL:               "https://etherscan.io/block/14746223",
		HttpHeader:        map[string]string{},
		ConnectionTimeout: 5,  //最大连接超时时长，默认30s，单位秒级别
		MaxRetry:          1,  //最大连接试错次数，默认3次
		MaxFetchTimeout:   10, //最大读取超时时长，默认10s，可以选择为10-60s

		FetchDataType:    "text",                 //json,xml,html,text
		FetchDataFormula: `<li>(.*)sub(.*)</li>`, //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据,https://goessner.net/articles/JsonPath/(json)
	}
	bs := smart_http.NewHttpRequest(vc)
	fmt.Println(string(bs))

	fetcher := new(FetchHttp)

	s1, err := fetcher.Fetch(string(bs))
	if err != nil {
		fmt.Printf("err is : %s ", err)
	}
	fmt.Println(string(s1))
}
