/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

// Package main 可执行文件入口
package main

import (
	"chainmaker-oracle/config"
	"chainmaker-oracle/contract_process"
	"chainmaker-oracle/contract_process/vrf_utils"
	"chainmaker-oracle/databases"
	"chainmaker-oracle/fetch_data"
	"chainmaker-oracle/http_server_process"
	"chainmaker-oracle/models"
	"errors"
	"flag"
	"fmt"
	"os"
	"time"
)

func main() {
	serverStartTime := time.Now()
	var defaultConfigPath string
	flag.StringVar(&defaultConfigPath, "i", "./config_files/smart_oracle.yml", "config file")
	flag.Parse()
	configErr := config.ReadConfigFile(defaultConfigPath) //配置
	if configErr != nil {
		_, _ = fmt.Fprintf(os.Stderr, "load cfg err %s ", configErr.Error())
		return
	}
	initDbErr := databases.InitDB(&config.GlobalCFG.OracleMysqlCFG) //初始化数据库连接数字
	if initDbErr != nil {
		_, _ = fmt.Fprintf(os.Stderr, "main init db error , %s  ", initDbErr.Error())
		return
	}
	if len(config.GlobalCFG.DataSourceMysqlCFG) > 0 {
		dataInitErr := fetch_data.InitDataSourceDB(config.GlobalCFG.DataSourceMysqlCFG)
		if dataInitErr != nil {
			_, _ = fmt.Fprintf(os.Stderr, "main init datasource err, %s  ", dataInitErr.Error())
		}
	}
	vrfErr := InitVRF()
	if vrfErr != nil {
		_, _ = fmt.Fprintf(os.Stderr, "InitVRF error(%s)  ", vrfErr.Error())
		return
	}
	thisProcessor, err := contract_process.NewContractProcessor()
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "main err, %s ", err.Error())
		return
	}

	_ = thisProcessor.RetrieveInProcessingJob(serverStartTime) //在这里加载未完成事件todo

	_ = thisProcessor.SubscribeContractEvents() //开始监听所有事件
	go thisProcessor.ConsumeEvents()            //消费事件信息
	go thisProcessor.WatchSubscribeClose()      // 监控关闭信号
	thisProcessor.MonitorContract()             //监控合约变化

	httpSrv := http_server_process.NewHttpSrv(thisProcessor)
	httpSrv.Listen()
	//接收到关闭信号,阻塞中
	thisProcessor.Stop()
	databases.CloseDB()          //关闭数据库连接
	fetch_data.CloseDataSource() //关闭额外数据源头
	// 关闭数据库连接
	_, _ = fmt.Fprintln(os.Stdout, "contract processor shutdown ")

}

// InitVRF 函数从数据库中查询是否有已经初始化的VRF公私钥信息，如果无则生成，插入数据库。用公私钥信息初始化VRF模块
// @return error
func InitVRF() error {
	loader := func() error {
		vrfDbData, vrfDbErr := models.QueryVRFStore()
		if vrfDbErr != nil {
			_, _ = fmt.Fprintf(os.Stderr, "InitVRF load info error (%s) ", vrfDbErr.Error())
			return vrfDbErr
		}
		if len(vrfDbData.PrivateKeys) > 0 {
			return vrf_utils.InitVRFProcessor(vrfDbData)
		}
		return errors.New("system not init")
	}
	err := loader()
	if err == nil {
		return nil
	}
	generateData, generateDataErr := vrf_utils.GenerateKeys()
	if generateDataErr != nil {
		_, _ = fmt.Fprintf(os.Stderr, "InitVRF GenerateKeys error (%s)  ", generateDataErr.Error())
		return generateDataErr
	}
	insertErr := models.SaveVRFStore(generateData)
	if insertErr != nil {
		_, _ = fmt.Fprintf(os.Stderr, "InitVRF SaveVRFStore error (%s)  ", insertErr.Error())
	}
	loadEr := loader()
	if loadEr != nil {
		return loadEr
	}
	return nil
}
