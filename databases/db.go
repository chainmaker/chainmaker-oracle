/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package databases

import (
	"chainmaker-oracle/config"
	"database/sql"
	"fmt"
	"os"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

var (
	GSmartOracleDB *sql.DB // GSmartOracleDB 预言机依赖的mysql数据源
)

const (
	globalSqlDNS = "%s:%s@tcp(%s)/%s?charset" +
		"=utf8mb4&loc=Local&parseTime=true" +
		"&interpolateParams=false" // globalSqlDNS 数据源sql的dns
)

// InitDB 初始化db
// @param *config.MySqlConfig
// @return error
func InitDB(systemCfg *config.MySqlConfig) error {
	var retErr error
	// 连接一下数据库
	GSmartOracleDB, retErr = initDataBase(systemCfg)
	if retErr != nil {
		// 连接失败了，50秒后重新连接一次
		time.Sleep(50 * time.Second)
		GSmartOracleDB, retErr = initDataBase(systemCfg)
		return retErr
	}

	return nil

}

// initDataBase 根据配置初始化db
// @param *config.MySqlConfig
// @return *sql.DB
// @return error
func initDataBase(cfg *config.MySqlConfig) (*sql.DB, error) {
	dbSourceName := fmt.Sprintf(globalSqlDNS,
		cfg.UserName, cfg.Password, cfg.DbAddress, cfg.Database)
	db, err := sql.Open("mysql", dbSourceName)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "init database(%s) got error : %s  ",
			cfg.Database, err.Error())
		return nil, err
	}
	// 设置一下最大空连接
	db.SetMaxIdleConns(cfg.MaxIdleConns)
	// 设置一下最大连接
	db.SetMaxOpenConns(cfg.MaxOpenConns)
	// 设置一下最大空连接时长
	db.SetConnMaxIdleTime(time.Duration(cfg.MaxIdleSeconds) * time.Second)
	// 设置一下最大连接时长
	db.SetConnMaxLifetime(time.Duration(cfg.MaxLifeSeconds) * time.Second)
	// ping一下数据库
	pErr := db.Ping()
	if pErr != nil {
		_, _ = fmt.Fprintf(os.Stderr, "ping database(%s) got error : %s  ",
			cfg.Database, pErr.Error())
		return nil, pErr
	}
	return db, nil
}

// CloseDB 关闭oracle数据源
func CloseDB() {
	_ = GSmartOracleDB.Close()

}
