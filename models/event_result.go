/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"context"
	"time"
)

const (
	insertEventResultStr = "insert into event_result(request_id, " +
		" event_result) values (?,?)" // insertEventResultStr 插入结果语句

	queryEventResultStr = "select request_id, " +
		"created_at,event_result from event_result where request_id = ? limit 1 " // queryEventResultStr 查询结果
)

// EventResult 查询数据结果存储
type EventResult struct {
	RequestId   string // 请求id，表的唯一主键
	CreatedAt   time.Time
	EventResult []byte // 查询结果
}

// InsertEventResult 保存结果
// @param *EventResult
// @return error
func InsertEventResult(data *EventResult) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_, sqlRetErr := databases.GSmartOracleDB.ExecContext(ctx, insertEventResultStr,
		data.RequestId, data.EventResult)
	if sqlRetErr != nil {

		return sqlRetErr
	}

	return nil
}

// QueryEventResult 查询结果
// @param string
// @return *EventResult
// @return error
func QueryEventResult(requestId string) (*EventResult, error) {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	sqlRows, sqlRowsErr := databases.GSmartOracleDB.QueryContext(ctx, queryEventResultStr, requestId)
	if sqlRowsErr != nil {

		return nil, sqlRowsErr
	}
	defer sqlRows.Close()
	if sqlRows.Next() {
		var temp EventResult
		scanErr := sqlRows.Scan(
			&temp.RequestId, &temp.CreatedAt, &temp.EventResult)
		if scanErr != nil {

			return nil, scanErr
		}
		return &temp, nil
	}
	if sqlRows.Err() != nil {
		return nil, sqlRows.Err()
	}
	return nil, nil
}
