/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"fmt"
	"testing"
	"time"
)

func TestInsertEventResult(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	dr := &EventResult{

		RequestId: "8d8c748287e396bf78e5b31b8f658fd0b",

		CreatedAt: time.Now(),

		EventResult: []byte("you"),
	}
	inErr := InsertEventResult(dr)
	if inErr != nil {
		fmt.Printf("insert error %s ", inErr.Error())
		return
	}

}

func TestQueryEventResult(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	dr := &EventResult{
		RequestId:   "8d8c748287e396bf78e5b31b8f658fd0b",
		CreatedAt:   time.Now(),
		EventResult: []byte("you"),
	}
	inErr := InsertEventResult(dr)
	if inErr != nil {
		fmt.Printf("insert error %s ", inErr.Error())
		return
	}
	res, resErr := QueryEventResult(dr.RequestId)
	if resErr != nil {
		fmt.Printf("query error %s ", resErr.Error())
		return
	}
	fmt.Printf("result is %+v ", res)
}
