/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

// Package models 定义了数据库表对应的结构体以及数据库操作
package models

import (
	"crypto/md5"
	"fmt"
	"io"
)

// ComputeHashKey 根据chainalias，txid，requestId计算md5
// @param string
// @param string
// @param string
// @return string
func ComputeHashKey(chainAlias, txId, requestId string) string {

	mdH := md5.New()
	_, err := io.WriteString(mdH, fmt.Sprintf("%s#%s#%s", chainAlias, txId, requestId))
	if err != nil {
		return fmt.Sprintf("%s#%s#%s", chainAlias, txId, requestId)
	}
	return fmt.Sprintf("%x", mdH.Sum(nil))
}
