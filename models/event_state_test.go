/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"chainmaker-oracle/models/smart_http"
	"fmt"
	"testing"
	"time"
)

func TestInsertEventState(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	vc := smart_http.ModelHttp{
		//Method:     "GET",
		URL:        "baidu.com",
		HttpHeader: map[string]string{"accept-coding": "gzip"},

		ConnectionTimeout: 30, //最大连接超时时长，默认30s，单位秒级别
		MaxRetry:          3,  //最大连接试错次数，默认3次
		MaxFetchTimeout:   10, //最大读取超时时长，默认10s，可以选择为10-60s

		FetchDataType:    "html",       //json,xml,html
		FetchDataFormula: ".form.name", //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据,https://goessner.net/articles/JsonPath/(json)
	}
	bs := smart_http.NewHttpRequest(&vc)
	insertErr := InsertEventState(&EventState{
		ChainAlias: "chain1",
		RequestId:  "8d8c748287e396bf78e5b31b8f658fd0",
		TxId:       "8d8c748287e396bf78e5b31b8f658fd0t01",
		State:      StateInit,
		QueryType:  QueryTypeHttp,
		QueryBody:  bs,
	})

	if insertErr != nil {
		fmt.Printf("insert db error , is %s  ", insertErr.Error())
		return
	}

}

func TestQueryInProcessEvents(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	vc := smart_http.ModelHttp{
		//Method:     "GET",
		URL:        "baidu.com",
		HttpHeader: map[string]string{"accept-coding": "gzip"},

		ConnectionTimeout: 30, //最大连接超时时长，默认30s，单位秒级别
		MaxRetry:          3,  //最大连接试错次数，默认3次
		MaxFetchTimeout:   10, //最大读取超时时长，默认10s，可以选择为10-60s

		FetchDataType:    "html",       //json,xml,html
		FetchDataFormula: ".form.name", //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据,https://goessner.net/articles/JsonPath/(json)
	}
	bs := smart_http.NewHttpRequest(&vc)
	insertErr := InsertEventState(&EventState{
		ChainAlias: "chain1",
		RequestId:  "8d8c748287e396bf78e5b31b8f658fd0",
		TxId:       "8d8c748287e396bf78e5b31b8f658fd0t01",
		State:      StateInit,
		QueryType:  QueryTypeHttp,
		QueryBody:  bs,
	})

	if insertErr != nil {
		fmt.Printf("insert db error , is %s  ", insertErr.Error())
		return
	}
	datas, datasErr := QueryInProcessEvents(time.Now(), 0)
	if datasErr != nil {
		fmt.Printf("query got error is : %s  ", datasErr.Error())
		return
	}
	fmt.Println("length : ", len(datas))
	for i, d := range datas {
		fmt.Printf("%d is : %+v, and query body is %s   ", i, d, string(d.QueryBody))
	}
}

func TestUpdateProcessEvent(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	vc := smart_http.ModelHttp{
		//Method:     "GET",
		URL:        "baidu.com",
		HttpHeader: map[string]string{"accept-coding": "gzip"},

		ConnectionTimeout: 30, //最大连接超时时长，默认30s，单位秒级别
		MaxRetry:          3,  //最大连接试错次数，默认3次
		MaxFetchTimeout:   10, //最大读取超时时长，默认10s，可以选择为10-60s

		FetchDataType:    "html",       //json,xml,html
		FetchDataFormula: ".form.name", //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据,https://goessner.net/articles/JsonPath/(json)
	}
	bs := smart_http.NewHttpRequest(&vc)
	insertErr := InsertEventState(&EventState{
		ChainAlias: "chain1",
		RequestId:  "8d8c748287e396bf78e5b31b8f658fd0",
		TxId:       "8d8c748287e396bf78e5b31b8f658fd0t01",
		State:      StateInit,
		QueryType:  QueryTypeHttp,
		QueryBody:  bs,
	})

	if insertErr != nil {
		fmt.Printf("insert db error , is %s  ", insertErr.Error())
		return
	}
	uRetErr := UpdateProcessEvent(&EventState{
		ChainAlias:    "chain1",
		RequestId:     "8d8c748287e396bf78e5b31b8f658fd0",
		TxId:          "8d8c748287e396bf78e5b31b8f658fd0t01",
		State:         StateInit,
		QueryType:     QueryTypeHttp,
		QueryBody:     bs,
		NextFibonacci: 200,
	})
	if uRetErr != nil {
		fmt.Printf("update got error , is %s  ", uRetErr.Error())
		return
	}
	datas, datasErr := QueryInProcessEvents(time.Now(), 0)
	if datasErr != nil {
		fmt.Printf("query got error is : %s  ", datasErr.Error())
		return
	}
	fmt.Println("length : ", len(datas))
	for i, d := range datas {
		fmt.Printf("%d is : %+v, and query body is %s   ", i, d, string(d.QueryBody))
	}
}

func TestQueryProcessEvent(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	vc := smart_http.ModelHttp{
		//Method:     "GET",
		URL:        "baidu.com",
		HttpHeader: map[string]string{"accept-coding": "gzip"},

		ConnectionTimeout: 30, //最大连接超时时长，默认30s，单位秒级别
		MaxRetry:          3,  //最大连接试错次数，默认3次
		MaxFetchTimeout:   10, //最大读取超时时长，默认10s，可以选择为10-60s

		FetchDataType:    "html",       //json,xml,html
		FetchDataFormula: ".form.name", //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据,https://goessner.net/articles/JsonPath/(json)
	}
	bs := smart_http.NewHttpRequest(&vc)
	insertErr := InsertEventState(&EventState{
		ChainAlias: "chain1",
		RequestId:  "8d8c748287e396bf78e5b31b8f658fd0b",
		TxId:       "8d8c748287e396bf78e5b31b8f658fd0t01",
		State:      StateInit,
		QueryType:  QueryTypeHttp,
		QueryBody:  bs,
	})

	if insertErr != nil {
		fmt.Printf("insert db error , is %s  ", insertErr.Error())
		return
	}
	d, dErr := QueryProcessEvent("8d8c748287e396bf78e5b31b8f658fd0")
	if dErr != nil {
		fmt.Println("query error : ", dErr.Error())
		return
	}
	fmt.Printf("%+v  ", d)
}
