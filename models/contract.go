/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"context"
	"time"
)

const (
	loadContractsStr = "select id,chain_alias, chain_id,contract_name,contract_version," +
		" deployed,runtime_type,topic_query_start,topic_finish_start,created_at ," +
		" updated_at from oracle_contracts" // loadContractsStr 查询合约

	insertContractStr = "insert into oracle_contracts(chain_id,chain_alias,contract_name," +
		" contract_version,deployed,runtime_type," +
		" topic_query_start,topic_finish_start) values(?,?,?,?,?,?,?,?) " // insertContractStr 插入合约

	upGradeContractStr = "update oracle_contracts set contract_version = ? " +
		"where chain_alias = ? and contract_name = ? and contract_version < ? " // upGradeContractStr 升级合约

	updateQueryStartStr = "update oracle_contracts set  topic_query_start = ? " +
		" where chain_alias = ? and contract_name = ? and topic_query_start < ? " // updateQueryStartStr 更新合约查询

	updateFinishStartStr = "update oracle_contracts set topic_finish_start = ? " +
		" where chain_alias = ? and contract_name = ? and topic_finish_start < ? " // updateFinishStartStr 更新合约完成

	queryContractByIdStr = "select id, chain_alias, chain_id, contract_name, contract_version," +
		"  deployed, runtime_type, topic_query_start, " +
		" topic_finish_start, created_at, updated_at from oracle_contracts " +
		" where chain_alias = ? and contract_name = ? limit 1 " // queryContractByIdStr 根据链+合约名称查询合约信息
)

// ContractInfo 合约信息模型
type ContractInfo struct {
	Id               uint64 // 主键
	ChainAlias       string // 链别名
	ChainId          string // 链id
	ContractName     string // 合约名称
	ContractVersion  int    // 合约版本
	Deployed         int    // 是否已经部署
	RuntimeType      int    // 6为docker-go版本
	TopicQueryStart  uint64 // oracle_query事件的监听起始位置
	TopicFinishStart uint64 // oracle_finish事件的监听起始位置
	CreatedAt        time.Time
	UpdatedAt        time.Time
}

// LoadContracts 查询所有合约
// @return []ContractInfo
// @return error
func LoadContracts() ([]ContractInfo, error) {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	sqlRows, sqlRowsErr := databases.GSmartOracleDB.QueryContext(ctx, loadContractsStr)
	if sqlRowsErr != nil {
		return nil, sqlRowsErr
	}
	defer sqlRows.Close()
	var retDataSlice []ContractInfo
	for sqlRows.Next() {
		var tempData ContractInfo
		scanErr := sqlRows.Scan(&tempData.Id, &tempData.ChainAlias, &tempData.ChainId, &tempData.ContractName,
			&tempData.ContractVersion, &tempData.Deployed, &tempData.RuntimeType,
			&tempData.TopicQueryStart, &tempData.TopicFinishStart, &tempData.CreatedAt, &tempData.UpdatedAt)
		if scanErr != nil {
			return nil, scanErr
		}
		retDataSlice = append(retDataSlice, tempData)
	}
	if sqlRows.Err() != nil {
		return nil, sqlRows.Err()
	}
	return retDataSlice, nil
}

// InsertContract 保存一个合约
// @param *ContractInfo
// @return error
func InsertContract(contract *ContractInfo) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_, sqlRetErr := databases.GSmartOracleDB.ExecContext(ctx, insertContractStr,
		contract.ChainId, contract.ChainAlias, contract.ContractName, contract.ContractVersion,
		contract.Deployed,
		contract.RuntimeType, contract.TopicQueryStart, contract.TopicFinishStart)
	if sqlRetErr != nil {

		return sqlRetErr
	}
	return nil
}

// UpGradeContractInfo 升级合约更新信息到数据库
// @param string
// @param string
// @param int
// @return error
func UpGradeContractInfo(chainAlias, contractName string, contractVersion int) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_, sqlRetErr := databases.GSmartOracleDB.ExecContext(ctx, upGradeContractStr, contractVersion,
		chainAlias, contractName, contractVersion)
	if sqlRetErr != nil {
		return sqlRetErr
	}

	return nil
}

// UpdateContractTopicQueryStart 更新合约监听的query事件的起始位置到数据库
// @param string
// @param string
// @param uint64
// @return error
func UpdateContractTopicQueryStart(chainAlias, contractName string, topicQueryStart uint64) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_, sqlRetErr := databases.GSmartOracleDB.ExecContext(ctx, updateQueryStartStr, topicQueryStart,
		chainAlias, contractName, topicQueryStart)
	if sqlRetErr != nil {
		return sqlRetErr
	}

	return nil
}

// UpdateContractTopicFinishStart 更新合约监听的finish事件的起始位置到数据库
// @param string
// @param string
// @param uint64
// @return error
func UpdateContractTopicFinishStart(chainAlias, contractName string, topicFinishStart uint64) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_, sqlRetErr := databases.GSmartOracleDB.ExecContext(ctx, updateFinishStartStr, topicFinishStart,
		chainAlias, contractName, topicFinishStart)
	if sqlRetErr != nil {
		return sqlRetErr
	}

	return nil
}

// QueryContractInfo 查询指定的合约信息
// @param string
// @param string
// @return *ContractInfo
// @return error
func QueryContractInfo(chainAlias, contractName string) (*ContractInfo, error) {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	sqlRet := databases.GSmartOracleDB.QueryRowContext(ctx, queryContractByIdStr,
		chainAlias, contractName)
	var contractInfo ContractInfo
	scanErr := sqlRet.Scan(&contractInfo.Id, &contractInfo.ChainAlias,
		&contractInfo.ChainId, &contractInfo.ContractName,
		&contractInfo.ContractVersion,
		&contractInfo.Deployed,
		&contractInfo.RuntimeType, &contractInfo.TopicQueryStart,
		&contractInfo.TopicFinishStart, &contractInfo.CreatedAt, &contractInfo.UpdatedAt)
	return &contractInfo, scanErr
}
