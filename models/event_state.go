/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"context"
	"time"
)

var (
	QueryStatePageSize = 100 // QueryStatePageSize 分页数据
)

const (
	queryEventStateStr = "select  id,chain_alias,request_id,tx_id,contract_name," +
		" state,query_type,query_body ,next_fibonacci ,block_height ," +
		" created_at,updated_at from event_state " +
		"where request_id = ? limit 1" // queryEventStateStr 查询事件状态

	updateEventStr = "update event_state set State = ?   , " +
		"next_fibonacci = ?  where request_id = ?  and state <= ?  " // updateEventStr 更新事件状态

	queryInProcessEventStr = "select id,chain_alias,request_id,tx_id,contract_name," +
		" state,query_type,query_body ,next_fibonacci ,block_height  ," +
		"created_at,updated_at from event_state where  updated_at < ? and state < ? " +
		" order by updated_at limit ? offset ?" // queryInProcessEventStr 查询尚未完成的事件，分页

	insertEventStateStr = "insert into event_state(chain_alias,request_id, " +
		" tx_id,contract_name,state,query_type,query_body , " +
		" next_fibonacci,block_height ) values (?,?,?,?,?,?,?,?,?)" // insertEventStateStr 插入事件状态到数据库
)

// EventState 事件状态存储
type EventState struct {
	Id            uint64           // 主键
	ChainAlias    string           // 链别名
	RequestId     string           // 请求id，唯一
	TxId          string           // 事务id
	ContractName  string           // 合约名称
	State         OracleEventState // 状态机状态
	QueryType     EventQueryType   // 查询类型
	NextFibonacci int64            // fibonacci数
	BlockHeight   int64            // 块高
	CreatedAt     time.Time
	UpdatedAt     time.Time
	QueryBody     []byte // 查询结构体
}

// InsertEventState 插入事件状态
// @param *EventState
// @return error
func InsertEventState(data *EventState) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_, sqlRetErr := databases.GSmartOracleDB.ExecContext(ctx, insertEventStateStr,
		data.ChainAlias, data.RequestId, data.TxId, data.ContractName,
		int8(data.State), int8(data.QueryType), data.QueryBody, 0, data.BlockHeight)
	if sqlRetErr != nil {

		return sqlRetErr
	}
	return nil
}

// QueryInProcessEvents 查询尚未完成事件
// @param time.Time
// @param int
// @param []EventState
// @return error
func QueryInProcessEvents(queryTime time.Time, page int) ([]EventState, error) {
	//page 从0 开始，每次查询gQueryStatePageSize
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	sqlRows, sqlRowsErr := databases.GSmartOracleDB.QueryContext(ctx, queryInProcessEventStr,
		queryTime.Format(time.RFC3339), int8(StateCallbackSuccess), QueryStatePageSize,
		QueryStatePageSize*page)
	if sqlRowsErr != nil {
		return nil, sqlRowsErr
	}

	defer sqlRows.Close()
	retData := make([]EventState, 0, QueryStatePageSize)
	for sqlRows.Next() {
		var tempData EventState
		scanErr := sqlRows.Scan(&tempData.Id, &tempData.ChainAlias, &tempData.RequestId,
			&tempData.TxId, &tempData.ContractName, &tempData.State, &tempData.QueryType,
			&tempData.QueryBody, &tempData.NextFibonacci,
			&tempData.BlockHeight, &tempData.CreatedAt, &tempData.UpdatedAt)
		if scanErr != nil {
			return nil, scanErr
		}
		retData = append(retData, tempData)
	}
	if sqlRows.Err() != nil {
		return nil, sqlRows.Err()
	}
	return retData, nil

}

// UpdateProcessEvent 更新事件状态
// @param *EventState
// @return error
func UpdateProcessEvent(data *EventState) error {
	//修改结构体中的state  next_fibonacci,为最新值，versions自动+1,locked是否上锁
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_, sqlResultErr := databases.GSmartOracleDB.ExecContext(ctx, updateEventStr,
		int8(data.State), data.NextFibonacci,
		data.RequestId, int8(data.State))
	if sqlResultErr != nil {
		return sqlResultErr
	}
	return nil
}

// QueryProcessEvent 根据请求id查询事件
// @param string
// @return *EventState
// @return error
func QueryProcessEvent(requestId string) (*EventState, error) {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	sqlRet := databases.GSmartOracleDB.QueryRowContext(ctx, queryEventStateStr,
		requestId)
	var retData EventState
	scanErr := sqlRet.Scan(&retData.Id, &retData.ChainAlias, &retData.RequestId,
		&retData.TxId, &retData.ContractName, &retData.State,
		&retData.QueryType, &retData.QueryBody,
		&retData.NextFibonacci, &retData.BlockHeight, &retData.CreatedAt, &retData.UpdatedAt)
	return &retData, scanErr
}
