/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"database/sql"
	"fmt"
	"time"

	"testing"

	_ "github.com/mattn/go-sqlite3"
)

const (
	gOracleContractTable = "CREATE TABLE  if NOT exists `oracle_contracts`(" +
		"`id` integer   PRIMARY KEY AUTOINCREMENT NOT NULL," +
		"`chain_id` varchar(128) NOT NULL ," +
		"`chain_alias` varchar(128) NOT NULL ," +
		"`contract_name` varchar(45) NOT NULL ," +
		"`contract_version` int NOT NULL DEFAULT '1' ," +
		"`deployed` tinyint NOT NULL DEFAULT '0' ," +
		"`runtime_type` tinyint NOT NULL DEFAULT '6' ," +
		"`topic_query_start` integer NOT NULL ," +
		"`topic_finish_start` integer NOT NULL ," +
		"`created_at` datetime DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime'))," +
		"`updated_at` datetime DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime'))," +
		"UNIQUE (`chain_alias`,`contract_name`));"
	gStateTable = "CREATE TABLE  if NOT exists `event_state`(" +
		"`id` integer   PRIMARY KEY AUTOINCREMENT NOT NULL," +
		"`chain_alias` varchar(128) NOT NULL," +
		"`request_id` varchar(256) NOT NULL," +
		"`tx_id` varchar(128) NOT NULL," +
		"`contract_name` varchar(45) NOT NULL," +
		"`state` tinyint NOT NULL DEFAULT '0'," +
		"`query_type` tinyint NOT NULL DEFAULT '1'," +
		"`query_body` blob NOT NULL," +
		"`next_fibonacci` integer NOT NULL DEFAULT '0'," +
		"`block_height` integer NOT NULL DEFAULT '1'," +
		"`created_at` datetime DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime'))," +
		"`updated_at` datetime DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime'))," +
		"UNIQUE (`request_id`));"

	gResultTable = "CREATE TABLE if NOT exists `event_result` (" +
		"`request_id` varchar(256) NOT NULL," +
		"`event_result` mediumblob NOT NULL," +
		"`created_at` datetime DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime'))," +
		"PRIMARY KEY (`request_id`));"
	gLockTable = "CREATE TABLE if NOT exists `event_lock` (" +
		"`request_id` varchar(256) NOT NULL," +
		"`locked_version` int NOT NULL DEFAULT '0'," +
		"`updated_at` datetime DEFAULT (datetime(CURRENT_TIMESTAMP,'localtime'))," +
		"PRIMARY KEY (`request_id`));"
	gvrfTable = "CREATE TABLE if NOT exists  `vrfs`(" +
		"`id` integer PRIMARY KEY AUTOINCREMENT NOT NULL," +
		"`privatek` blob NOT NULL," +
		"`publick` blob NOT NULL," +
		"`srv_name` varchar(128) NOT NULL," +
		"UNIQUE (`srv_name`));"
)

func initSqlite3() *sql.DB {
	database, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		return nil
	}
	database.Exec(gOracleContractTable)
	database.Exec(gStateTable)
	database.Exec(gResultTable)
	database.Exec(gLockTable)
	database.Exec(gvrfTable)
	return database
}

func TestInsertContract(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	mockData := &ContractInfo{
		ChainId:          "chain1",
		ContractName:     "oracle_contract_v1",
		ContractVersion:  1,
		Deployed:         1,
		RuntimeType:      6,
		TopicQueryStart:  1,
		TopicFinishStart: 1,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}

	insertErr := InsertContract(mockData)
	if insertErr != nil {
		fmt.Printf("test insert error , %s ", insertErr.Error())
		return
	}

	insertErr2 := InsertContract(mockData)
	if insertErr2 != nil {
		fmt.Printf("test insert error , %s ", insertErr2.Error())
		return
	}

	infos, infosErr := LoadContracts()
	if infosErr != nil {
		fmt.Printf("loadContracts error , %s ", infosErr.Error())
		return
	}
	fmt.Printf("datas : %+v ", infos)
}

func TestLoadContracts(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	mockData := &ContractInfo{
		ChainId:          "chain1",
		ContractName:     "oracle_contract_v1",
		ContractVersion:  1,
		Deployed:         1,
		RuntimeType:      6,
		TopicQueryStart:  1,
		TopicFinishStart: 1,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
	insertErr := InsertContract(mockData)
	if insertErr != nil {
		fmt.Printf("test insert error , %s ", insertErr.Error())
		return
	}

	infos, infosErr := LoadContracts()
	if infosErr != nil {
		fmt.Printf("loadContracts error , %s ", infosErr.Error())
		return
	}
	fmt.Printf("datas : %+v ", infos)
}

func TestUpGradeContractInfo(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	mockData := &ContractInfo{
		ChainId:          "chain1",
		ContractName:     "oracle_contract_v1",
		ContractVersion:  1,
		Deployed:         1,
		RuntimeType:      6,
		TopicQueryStart:  1,
		TopicFinishStart: 1,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
	insertErr := InsertContract(mockData)
	if insertErr != nil {
		fmt.Printf("test insert error , %s ", insertErr.Error())
		return
	}

	successErr := UpGradeContractInfo(mockData.ChainId, mockData.ContractName, 2)
	if successErr != nil {
		fmt.Printf("test upgrade error, %s ", successErr.Error())
		return
	}

	infos, _ := LoadContracts()
	fmt.Printf("%+v  ", infos)
}

func TestUpdateContractTopicQueryStart(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	mockData := &ContractInfo{
		ChainId:          "chain1",
		ContractName:     "oracle_contract_v1",
		ContractVersion:  1,
		Deployed:         1,
		RuntimeType:      6,
		TopicQueryStart:  1,
		TopicFinishStart: 1,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
	insertErr := InsertContract(mockData)
	if insertErr != nil {
		fmt.Printf("test insert error , %s ", insertErr.Error())
		return
	}

	successErr := UpdateContractTopicQueryStart(mockData.ChainId, mockData.ContractName, 200)
	if successErr != nil {
		fmt.Printf("test querystart error, %s ", successErr.Error())
		return
	}

	infos, _ := LoadContracts()
	fmt.Printf("%+v  ", infos)
}

func TestUpdateContractTopicFinishStart(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	mockData := &ContractInfo{
		ChainId:          "chain1",
		ContractName:     "oracle_contract_v1",
		ContractVersion:  1,
		Deployed:         1,
		RuntimeType:      6,
		TopicQueryStart:  1,
		TopicFinishStart: 1,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
	insertErr := InsertContract(mockData)
	if insertErr != nil {
		fmt.Printf("test insert error , %s ", insertErr.Error())
		return
	}

	successErr := UpdateContractTopicFinishStart(mockData.ChainId, mockData.ContractName, 200)
	if successErr != nil {
		fmt.Printf("test queryfinish error, %s ", successErr.Error())
		return
	}
	infos, _ := LoadContracts()
	fmt.Printf("%+v  ", infos)
}

func TestQueryContractInfo(t *testing.T) {
	databases.GSmartOracleDB = initSqlite3()
	defer databases.CloseDB()
	mockData := &ContractInfo{
		ChainId:          "chain1",
		ContractName:     "oracle_contract_v1",
		ContractVersion:  1,
		Deployed:         1,
		RuntimeType:      6,
		TopicQueryStart:  1,
		TopicFinishStart: 1,
		CreatedAt:        time.Now(),
		UpdatedAt:        time.Now(),
	}
	insertErr := InsertContract(mockData)
	if insertErr != nil {
		fmt.Printf("test insert error , %s ", insertErr.Error())
		return
	}
	info, infoErr := QueryContractInfo(mockData.ChainId, mockData.ContractName)
	if infoErr != nil {
		fmt.Printf("query error , %s ", infoErr.Error())
		return
	}
	fmt.Printf("%+v  ", info)
}
