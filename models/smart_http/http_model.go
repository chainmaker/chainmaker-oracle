/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

// Package smart_http 定义了用户查询http接口数据的模型
package smart_http

import (
	"encoding/json"
)

// ModelHttp 用户查询http接口数据模型
type ModelHttp struct {
	URL               string            `json:"url"`
	HttpHeader        map[string]string `json:"http_header"`
	HttpBody          []byte            `json:"http_body"`
	ConnectionTimeout int               `json:"connection_timeout"` //最大连接超时时长，默认30s，单位秒级别
	MaxRetry          int               `json:"max_retry"`          //最大连接试错次数，默认3次
	MaxFetchTimeout   int               `json:"max_fetch_timeout"`  //最大读取超时时长，默认10s，可以选择为10-60s
	FetchDataType     string            `json:"fetch_data_type"`    //json,xml,html
	FetchDataFormula  string            `json:"fetch_data_formula"` //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据
}

// NewHttpRequest 序列化一个用户查询http接口数据模型为json
// @param *ModelHttp
// @return []byte
func NewHttpRequest(cfg *ModelHttp) []byte {
	bs, _ := json.Marshal(cfg)
	return bs
}
