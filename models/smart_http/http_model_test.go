/*
Copyright (C) BABEC. All rights reserved.
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package smart_http

import (
	"fmt"
	"testing"
)

func TestNewHttpRequest(t *testing.T) {
	vc := ModelHttp{
		//Method:     "GET",
		URL:        "baidu.com",
		HttpHeader: map[string]string{"accept-coding": "gzip"},

		ConnectionTimeout: 30, //最大连接超时时长，默认30s，单位秒级别
		MaxRetry:          3,  //最大连接试错次数，默认3次
		MaxFetchTimeout:   10, //最大读取超时时长，默认10s，可以选择为10-60s

		FetchDataType:    "html",       //json,xml,html
		FetchDataFormula: ".form.name", //默认为空，为空则不解析，原样返回。如果非空，则按照. 来分隔取出来结构化的数据,https://goessner.net/articles/JsonPath/(json)
	}
	bs := NewHttpRequest(&vc)
	fmt.Println(string(bs))
}
