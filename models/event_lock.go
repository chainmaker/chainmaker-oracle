/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"context"
	"errors"
	"time"
)

const (
	queryLockStr = "select  request_id, locked_version," +
		" updated_at from event_lock " +
		"where request_id = ? limit 1" // queryLockStr 查询锁信息
	updateLockStr = "update event_lock set locked_version = locked_version+1 " +
		" where request_id = ? " // updateLockStr 更新锁版本

	updateGetLockStr = "update event_lock set locked_version = locked_version+1 " +
		" where request_id = ? and updated_at < ?" // updateGetLockStr 拿锁
	insertLockStr = "insert into event_lock(request_id," +
		" locked_version) values (?,?)" // insertLockStr 插入锁
)

// EventLock 锁模型
type EventLock struct {
	RequestId     string // 请求唯一主键
	LockedVersion int    // 版本
	UpdatedAt     time.Time
}

// InsertEventLock 插入锁
// @param *EventLock
// @return error
func InsertEventLock(data *EventLock) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_, sqlRetErr := databases.GSmartOracleDB.ExecContext(ctx, insertLockStr,
		data.RequestId, data.LockedVersion)
	if sqlRetErr != nil {
		return sqlRetErr
	}
	return nil
}

// UpdateEventLock 更新锁版本
// @param *EventLock
// @return error
func UpdateEventLock(data *EventLock) error {
	//上锁
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	sqlResult, sqlResultErr := databases.GSmartOracleDB.ExecContext(ctx, updateGetLockStr,
		data.RequestId, data.UpdatedAt.Format(time.RFC3339))
	if sqlResultErr != nil {
		return sqlResultErr
	}
	affected, affectedErr := sqlResult.RowsAffected()
	if affectedErr != nil {
		return affectedErr
	}
	if affected < 1 {
		return errors.New("update lock failed")
	}
	return nil
}

// UpdateLock 拿锁
// @param *EventLock
// @return error
func UpdateLock(data *EventLock) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	sqlResult, sqlResultErr := databases.GSmartOracleDB.ExecContext(ctx, updateLockStr,
		data.RequestId)
	if sqlResultErr != nil {
		return sqlResultErr
	}
	affected, affectedErr := sqlResult.RowsAffected()
	if affectedErr != nil {
		return affectedErr
	}
	if affected < 1 {
		return errors.New("update lock failed")
	}
	return nil
}

// QueryEventLock 查锁
// @param string
// @return *EventLock
// @return error
func QueryEventLock(requestId string) (*EventLock, error) {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	sqlRet := databases.GSmartOracleDB.QueryRowContext(ctx,
		queryLockStr,
		requestId)
	var retData EventLock
	scanErr := sqlRet.Scan(&retData.RequestId,
		&retData.LockedVersion, &retData.UpdatedAt)
	return &retData, scanErr
}
