/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

// Package cross_chain 定义了合约跨链查询模型
package cross_chain

// CrossChainQuery 合约跨链查询模型
type CrossChainQuery struct {
	ChainAlias   string         `json:"chain_alias"` //调用的链id
	ContractName string         `json:"contract_name"`
	MethodName   string         `json:"method_name"`
	Params       []KeyValuePair `json:"params"`
}

// KeyValuePair 合约传参数模型
type KeyValuePair struct {
	Key   string `json:"key"`
	Value []byte `json:"value"`
}
