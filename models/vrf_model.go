/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

import (
	"chainmaker-oracle/databases"
	"context"
	"time"
)

const (
	vrfInsertStr = `insert into vrfs(privatek,publick,srv_name ) values (?,?,?)` // vrfInsertStr 数据模型保存语句
	vrfQueryStr  = "select privatek , publick , srv_name from vrfs limit 1; "    // vrfQueryStr 数据模型查询语句
)

var (
	srvNameUniq = "chainmaker-oracle" // srvNameUniq 服务名称
)

// VRFStore VRF公司钥数据模型
type VRFStore struct {
	PrivateKeys []byte // 私钥
	PublicKeys  []byte // 公钥
	SrvName     string // 服务名称，唯一键
}

// QueryVRFStore 从数据库中查询VRF公私钥信息
// @return *VRFStore
// @return error
func QueryVRFStore() (*VRFStore, error) {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	sqlRows := databases.GSmartOracleDB.QueryRowContext(ctx, vrfQueryStr)

	var retData VRFStore
	scanErr := sqlRows.Scan(&retData.PrivateKeys, &retData.PublicKeys, &retData.SrvName)
	if scanErr != nil {
		return nil, scanErr
	}
	return &retData, nil

}

// SaveVRFStore 保存VRF公私钥数据到数据库
// @param *VRFStore
// @return error
func SaveVRFStore(data *VRFStore) error {
	ctx, ctxCancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer ctxCancel()
	_, sqlRetErr := databases.GSmartOracleDB.ExecContext(ctx, vrfInsertStr,
		data.PrivateKeys, data.PublicKeys, srvNameUniq)
	if sqlRetErr != nil {

		return sqlRetErr
	}
	return nil
}

// Random 随机数模型
type Random struct {
	RandData string  `json:"rand_data"` // 随机数
	Pi       []byte  `json:"pi"`        // 证据
	Ratio    float64 `json:"ratio"`     // [0,1]概率
}

// Verify 可验证随机书证据
type Verify struct {
	Pi    []byte `json:"pi"`    // 证据
	Alpha string `json:"alpha"` // 原始信息
}
