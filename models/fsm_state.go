/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

package models

// OracleEventState 状态枚举
type OracleEventState int8

const (
	StateInit                    OracleEventState = iota // StateInit 初始状态
	StateFetchDataFailed                                 // StateFetchDataFailed 获取事件失败
	StateFetchDataSuccess                                // StateFetchDataSuccess 获取数据成功
	StateCallbackFailed                                  // StateCallbackFailed 预言机器回调oracle合约失败
	StateCallbackSuccess                                 // StateCallbackSuccess 预言机器回调oracle合约成功
	StateResultOnChainSuccess                            // StateResultOnChainSuccess 回调数据上链成功
	StateCallbackFailedFibonacci                         // StateCallbackFailedFibonacci 最终回调链上数据失败状态，终止态之一
)
